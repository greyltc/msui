# msui
A Measurement System User Interface

## Testing setup
```
mkdir -p "${HOME}/measurement_config.d"
cp /usr/share/msui/configs/60_do_virt.yaml ${HOME}/measurement_config.d/"
runpanel
```

## Development workflow
1) Use git to clone this repo and cd into its folder
1) Install dependancies system-wide using your favorite python package manager. View those like this:
    ```bash
    $ hatch project metadata | jq -r '.dependencies | .[]'
    ```
1) Setup a virtual environment for development/testing
    ```bash
    $ python -m venv --without-pip --system-site-packages --clear venv
    ```
1) Activate the venv (this step is os/shell-dependant, see [1] for non-linux/bash)
    ```bash
    $ source venv/bin/activate
    ```
1) Install the package in editable mode into the venv
    ```bash
    (venv) $ python tools/venv_dev_install.py
    ```
1) Develop! When you're finished with it, you can deactivate the virtual environment with `deactivate`

## GSettings stuff
one time setup for GSettings
```
sudo cp data/gsettings/* /usr/share/glib-2.0/schemas/
sudo glib-compile-schemas /usr/share/glib-2.0/schemas/
```
get the settings keys:
```
gsettings list-keys org.greyltc.msui
```
get a key's value:
```
gsettings get org.greyltc.msui db-address
gsettings get org.greyltc.msui conf-id
```
set a key's value:
```
gsettings set org.greyltc.msui db-address postgresql:///testing
gsettings set org.greyltc.msui conf-id 1
```
