import unittest
from msui.sysdbus import Sysdbus


class SysdbusTestCase(unittest.TestCase):
    """systemd dbus interface testing code"""

    bus_name = "org.freedesktop.systemd1"

    def test_init(self):
        """check that we can init"""
        sdb = Sysdbus(bus_name=self.bus_name)
        self.assertIsInstance(sdb, Sysdbus)

    def test_connect(self):
        """test creation of systemd dbus proxy connection"""
        bn = self.bus_name
        bn = "org.greyltc.testing"
        sdb = Sysdbus(bus_name=bn)
        sdb.connect()
        self.assertTrue(sdb.ready)

    def test_start_serivce(self):
        """test start service"""
        bn = self.bus_name
        sdb = Sysdbus(bus_name=bn)
        sdb.connect()
        job = sdb.start_service("systemd-tmpfiles-setup.service")
        self.assertIn(bn.replace(".", "/"), job)

    def test_list_units(self):
        """test unit listing"""
        bn = self.bus_name
        sdb = Sysdbus(bus_name=bn)
        sdb.connect()
        units = sdb.list_units()
        unit_names = [unitline[0] for unitline in units]
        unit_name_to_check = "dbus.service"
        self.assertIn(unit_name_to_check, unit_names)

    def test_is_running(self):
        """test unit running checker"""
        bn = self.bus_name
        sdb = Sysdbus(bus_name=bn)
        sdb.connect()
        self.assertTrue(sdb.check_sub_state("dbus.service"))
