import unittest
from msui.gui import App
import asyncio


class RunpanelTestCase(unittest.TestCase):
    """some runpanel gui testing stuff"""

    def test_init(self):
        """check if init works"""
        app = App()
        self.assertIsInstance(app, App)

    def test_gui(self):
        """check that gui works (requires user interaction to quit)"""
        app = App()
        app.run()

    def test_run_message_capture(self):
        async def your_mom(message):
            lp = asyncio.get_running_loop()
            lp.create_datagram_endpoint

            reader = StreamReader(limit=limit, loop=loop)
            protocol = StreamReaderProtocol(reader, loop=loop)
            transport, _ = await loop.create_connection(lambda: protocol, host, port, **kwds)
            writer = StreamWriter(transport, protocol, reader, loop)
            reader, writer = await asyncio.open_connection("<broadcast>", 54787)
