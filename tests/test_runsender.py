import unittest
from msui.run_sender import RunSender
import tempfile
import json
from pathlib import Path
import textwrap

# import pandas as pd


class RunsenderCase(unittest.TestCase):
    """testing for non-gui run initaitor"""

    def test_init(self):
        """check that we can init"""

        rs = RunSender()
        self.assertIsInstance(rs, RunSender)

    def test_send(self):
        """tests initiating a run"""

        tsv_str = """
            system_label	user_label	layout	Variable	bitmask
            A	john	one large		0x3f"""
        tsv_bytes = textwrap.dedent(tsv_str).lstrip().encode()

        args = {}
        args["enable_iv"] = True
        args["IV_stuff"] = []

        config = {}
        config["stage"] = {"experiment_positions": {"solarsim": (0.0, 0.0)}}

        run_dict = {"rundata": {"args": args, "config": config, "digest": None}}
        jbytes = json.dumps(run_dict).encode()

        rs = RunSender()
        with tempfile.NamedTemporaryFile() as stsv:
            stsv.write(tsv_bytes)
            stsv.flush()
            rs.slots_file = Path(stsv.name)
            with tempfile.NamedTemporaryFile() as jfp:
                jfp.write(jbytes)
                jfp.flush()
                # rs.run_file = Path(jfp.name)
                rs.run_file = Path("/home/grey/states/gui_state2.gsf.json")
                ret_code = rs.run()
                self.assertEqual(ret_code, 0)
