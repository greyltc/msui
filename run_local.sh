#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd "${DIR}/src"

GTK_THEME=Adwaita:light PYTHONUSERBASE="${DIR}/src" python -m msui
