#!/usr/bin/env python

import msui
import gi

gi.require_versions({"WebKit2": "4.0", "GLib": "2.0", "Gio": "2.0", "Rsvg": "2.0", "Gtk": "3.0"})

from gi.repository import GLib, Gio, Gtk, Gdk, Rsvg, Pango  # type: ignore

# Gdk.set_allowed_backends('broadway')  # for gui over web
from gi.repository.WebKit2 import WebView  # type: ignore # these are used via ui.glade

try:
    from centralcontrol.logstuff import get_logger as getLogger
except:
    from logging import getLogger

import pathlib
import importlib.resources
import importlib.metadata
import uuid
from packaging import version as pversion

import logging
import pprint
import matplotlib
import threading
import csv

from msui.register_config import ConfigRegisterer
import redis
import json
import hmac

# import cairo

from urllib.parse import urlparse, parse_qs

# for drawing layouts
import drawsvg

import sys
import os
import time
import math
import humanize
import datetime as dt
import paho.mqtt.client as mqtt

# import numpy as np
from io import BytesIO
import socket

import re
import warnings

matplotlib.use("svg")  # or 'GTK3Cairo'?
import matplotlib.pyplot as plt

# for service management
from msui.sysdbus import Sysdbus
from msui.common import get_things_to_measure, new_run


class App(Gtk.Application):
    id: str = "org.greyltc.msui.runpanel"
    ui_filename: str = "ui.glade"
    mem_db_url: str | None = None
    cli_mem_db_url: None | str = None
    cli_conf_paths: None | list[pathlib.Path] = None
    conf_a: None | dict = None  # merged configuration data from the config files
    conf_a_id: str | None = None
    main_win = None
    ticker_id: int | None = None
    mqttc: mqtt.Client | None = None
    mqtt_thread: threading.Thread | None = None
    layouts: dict[str, dict[str, list]] = {}  # dict keeping track of active layouts
    layout_drawings: dict[str, tuple[drawsvg.drawing.Drawing, drawsvg.drawing.Drawing]] = {}
    slots: dict = {}  # dict keeping track of the slots
    group_order: list[list[list]] = []
    # to keep track of the two toggle buttons in the utility panel
    all_mux_switches_open = True
    in_iv_mode = True
    run_handler_status = "Unknown"
    iv_cal_time = None
    array_drawing_handle = None
    layout_drawing_handle = None
    spectrum_plot_handle = None
    want_spectrum = False
    terminating = False
    mc_sock = None
    json_dump: bool = False  # enable json dumps to disk at run start
    multicast_group = "224.0.0.153"  # unrouteable (local subnet only)
    multiacst_port = 33578
    multicast_ttl = 0  # packet time to live in hops
    live_data_webviews_loaded_once = False  # crap to prevent problematic sizing of webviews
    custom_webview_loaded_once = False  # crap to prevent problematic sizing of webviews
    known_window_height = None  # so we can keep track of the window changing vertical size
    hk = "gosox".encode()
    variables: list[str] = []  # names of user supplied variables
    slot_table_base_headings: list[str] = []  # names of the built-in slot table cols

    def __init__(self, *args, **kwargs):
        # needed to filter out a stupid bug-warning in gio
        # https://bugzilla.gnome.org/show_bug.cgi?id=708676
        # that shows up on input validation
        warnings.filterwarnings("ignore", ".*g_value_get_int.*G_VALUE_HOLDS_INT.*", Warning)

        self.lg = getLogger(".".join([__name__, type(self).__name__]))  # setup logging

        # super constructor
        super().__init__(*args, application_id=self.id, flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE, **kwargs)

        # allow configuration file location to be specified by command line argument
        self.add_main_option("max-runtime", ord("r"), GLib.OptionFlags.HIDDEN, GLib.OptionArg.DOUBLE, "Close the program after this many seconds", None)
        self.add_main_option("mem-db-url", ord("m"), GLib.OptionFlags.HIDDEN, GLib.OptionArg.STRING, "Memory database connection string", None)
        self.add_main_option("conf-file", ord("c"), GLib.OptionFlags.NONE, GLib.OptionArg.FILENAME_ARRAY, "Configuration file(s)", None)
        self.add_main_option(GLib.OPTION_REMAINING, ord("c"), GLib.OptionFlags.NONE, GLib.OptionArg.FILENAME_ARRAY, "Configuration file(s)", None)

        # socket for multicast comms
        self.mc_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        self.mc_sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, self.multicast_ttl)

    def do_startup(self):
        self.lg.debug(f"Starting up app from {__file__}")
        Gtk.Application.do_startup(self)

        # load the UI xml file
        self.b = Gtk.Builder.new_from_string(importlib.resources.files(msui).joinpath(self.ui_filename).read_text(), -1)

        # crawl the builder object and mine all the object ID strings we can
        # this list of id strings will need to be filtered by the load and save functions
        # a good starting point will be to discard those starting with "___"
        # I should probably just rename all the saveable/loadable stuff to contain
        # some special character so the filtering becomes easier
        self.ids = []
        for o in self.b.get_objects():
            if isinstance(o, Gtk.Buildable):
                self.ids.append(Gtk.Buildable.get_name(o))

    def emit_message(self, *args, **kwargs):
        """
        emits a message on various channels for external consumption/IPC
        takes args like mqtt client publish() or a single positional argument to send
        """
        mc_datagram = {}  # for multicast emission
        if (len(args) == 2) or ("payload" in kwargs):  # assume mqttc.publish format was given
            mqtt_pub_kws = {}
            channel = args[0]
            mc_datagram["channel"] = channel
            if "payload" in kwargs:
                payload = kwargs["payload"]
            else:
                payload = args[1]
            mc_datagram["payload"] = payload
            if "qos" in kwargs:
                mqtt_pub_kws["qos"] = kwargs["qos"]
            if self.mqttc:
                self.mqttc.publish(channel, payload, **mqtt_pub_kws)
        elif len(args) == 1:  # assume just payload was given, then it's only a mc emission
            channel = None
            mc_datagram["channel"] = channel
            payload = args[0]
            mc_datagram["payload"] = payload
        else:
            self.lg.debug("Unexpected emit_message call format")
            return

        # TODO: consider hmac to prevent message tampering
        # TODO: possibly add header to udp multicast --> struct of bytes_long(uint32):packet_counter(uint16):CRC16
        mc_bytes = json.dumps(mc_datagram).encode()
        if self.mc_sock:
            try:
                self.mc_sock.sendto(mc_bytes, (self.multicast_group, self.multiacst_port))  # multicast
            except Exception as e:
                self.lg.debug("multicast fail")
            # TODO: possibly add footer to UDP multicast --> CRC16

        # redis emission
        if self.mem_db_url:
            with redis.Redis.from_url(self.mem_db_url) as r:
                r.xadd(f"util:{channel}", fields={"payload": payload}, maxlen=100, approximate=True)

    def do_activate(self):
        self.lg.debug("Activating app")

        # We only allow a single window and raise any existing ones
        if self.main_win is None:
            # Windows are associated with the application
            # when the last one is closed the application shuts down

            self.hb = self.b.get_object("headerBar")

            self.logTB = self.b.get_object("tbLog")  # log text buffer
            self.log_win_adj = self.b.get_object("vert_log_win_scroll_adj")
            # drawings/plots
            self.array_pic = self.b.get_object("array_overview")
            self.array_pic.connect("draw", self.on_array_pic_draw)
            self.subs_pic = self.b.get_object("substrate_pic")
            self.subs_pic.connect("draw", self.on_subs_pic_draw)

            # setup logging to the UI log pane
            def myWrite(buf):
                # the log update should not be done on the main gui thread
                # or else segfault badness
                str_buf = str(buf)
                GLib.idle_add(self.append_to_log_window, str_buf)

            def myFlush():
                pass

            self.logTB.write = myWrite
            self.logTB.flush = myFlush
            uiLog = logging.StreamHandler(stream=self.logTB)
            uiLog.setLevel(logging.INFO)
            uiLog.set_name("ui")
            uiLogFormat = logging.Formatter(("%(asctime)s|%(levelname)s|%(message)s"))
            uiLog.setFormatter(uiLogFormat)
            self.lg.addHandler(uiLog)
            logging.addLevelName(29, "GUINFO")
            self.lg.debug("Gui logging setup.")

            try:
                self.enable_eqe = False
                cr_init_args = {}
                if self.cli_mem_db_url:
                    cr_init_args["mem_db_url"] = self.cli_mem_db_url
                if self.cli_conf_paths:
                    cr_init_args["conf_paths"] = self.cli_conf_paths

                cr = ConfigRegisterer(**cr_init_args)
                self.mem_db_url = cr.mem_db_url
                cr.note = "from gui launch"
                if cr.run() < 0:
                    raise RuntimeError(f"Failure registering config type A")
                self.conf_a_id = cr.idx
                if not self.conf_a_id:
                    raise RuntimeError(f"Failure type 2 registering config type A")
                self.conf_a = cr.conf_a
                if not self.conf_a:
                    raise RuntimeError(f"Failure loading")
                self.lg.log(29, f"Registered configuration ID number {self.conf_a_id}")

                to_show = []
                to_show.append("run_but")
                to_show.append("save_but")
                to_show.append("stop_but")
                to_show.append("load_but")
                to_show.append("load_last")
                to_show.append("main_stack")
                to_show.append("live_stack")
                to_show.append("utils_stack")
                to_show.append("array_overview")
                for o in to_show:
                    self.b.get_object(o).set_visible(True)
                # except RuntimeError:
                #     raise
                # except Exception as e:  # TODO: consider wrapping a lot more in this try to ensure the gui dosn't crash on bad config
                #     raise RuntimeError(f"Failure loading main configuration: {repr(e)}")

                self.lg.setLevel(self.conf_a["meta"]["internal_loglevel"])
                for h in self.lg.handlers:  # look for the UI log handler
                    if h.get_name() == "ui":
                        h.setLevel(self.conf_a["meta"]["ui_loglevel"])  # set its level

                # print the full config to the log
                pp = pprint.PrettyPrinter(compact=True, width=140, sort_dicts=False)
                self.lg.debug("Final merged config follows")
                self.lg.debug(pp.pformat(self.conf_a))

                try:
                    self.b.get_object("user_name_frame").set_visible(self.conf_a["UI"]["show_user_box"])
                except:
                    pass

                try:
                    self.b.get_object("custom_wv").set_visible(self.conf_a["UI"]["show_custom_tab"])
                except:
                    pass

                try:
                    self.b.get_object("livechart_btn").set_visible(self.conf_a["UI"]["show_alpha_btn"])
                except:
                    pass

                try:
                    if self.conf_a["UI"]["show_stage_cal"] == True:
                        self.b.get_object("recal_but").set_visible(True)
                except Exception as e:
                    pass

                try:
                    self.debug = self.conf_a["meta"]["debug"] == True
                except Exception as e:
                    self.debug = False

                try:
                    self.show_dumb = self.conf_a["UI"]["show_dumb"] == True
                except Exception as e:
                    self.show_dumb = False

                try:
                    self.show_sweep_deets = self.conf_a["UI"]["show_sweep_deets"] == True
                except Exception as e:
                    self.show_sweep_deets = False

                try:
                    self.show_analysis_tab = self.conf_a["UI"]["show_analysis_tab"] == True
                except Exception as e:
                    self.show_analysis_tab = False

                try:
                    self.restrict_nplc = self.conf_a["UI"]["restrict_nplc"] == True
                except Exception as e:
                    self.restrict_nplc = False

                try:
                    self.show_conn = self.conf_a["UI"]["show_conn_btn"] == True
                except Exception as e:
                    self.show_conn = False

                self.mqtt_server = self.conf_a["network"]["MQTTHOST"]

                # get ready to start some services
                sdb = Sysdbus()
                sdb.connect()
                self.services_to_launch = []
                self.services_to_launch.append(f"iv-plotter@{self.mqtt_server}.service")
                self.services_to_launch.append(f"vt-plotter@{self.mqtt_server}.service")
                self.services_to_launch.append(f"it-plotter@{self.mqtt_server}.service")
                self.services_to_launch.append(f"mppt-plotter@{self.mqtt_server}.service")

                # should we launch a local saver?
                try:
                    self.start_local_saver = self.conf_a["start_local_saver"] == True
                except:
                    self.start_local_saver = False

                if self.start_local_saver == True:
                    self.services_to_launch.append(f"mqtt-saver@{self.mqtt_server}.service")

                # should we launch a local measurement server?
                try:
                    self.start_local_server = self.conf_a["start_local_server"] == True
                except:
                    self.start_local_server = False

                if self.start_local_server == True:
                    self.services_to_launch.append(f"centralcontrol@{self.mqtt_server}.service")

                # are we using a daq here?
                try:
                    self.enable_daq = self.conf_a["daq"]["enabled"] == True
                except:
                    self.enable_daq = False

                # do we have a pcb controller?
                try:
                    self.enable_controller = self.conf_a["mc"]["enabled"] == True
                except Exception as e:
                    self.enable_controller = False

                # do we have a mux controller?
                try:
                    self.enable_mux = self.conf_a["mux"]["enabled"] == True
                except Exception as e:
                    self.enable_mux = False

                # do we have a z stage here?
                try:
                    self.enable_zee = self.conf_a["motion"]["zee"]["enabled"] == True
                except Exception as e:
                    self.enable_zee = False

                if self.enable_zee == True:
                    # read the default zee pos from the config and set the gui box to that
                    if "presets" in self.conf_a["motion"]["zee"]:
                        ze = self.b.get_object("zee_entry")
                        zc = self.b.get_object("zee_combo")
                        i = 0
                        for key, val in self.conf_a["motion"]["zee"]["presets"].items():
                            if i == 0:
                                ze.set_text(key)
                            zc.append_text(key)
                            i += 1
                else:
                    zbox = self.b.get_object("zbox")
                    zbox.set_visible(False)

                # are we using a stage controller here?
                try:
                    self.enable_stage = self.conf_a["motion"]["enabled"] == True
                except:
                    self.enable_stage = False

                # handle custom locations and stage stuff
                if self.enable_stage == True:
                    pl = self.b.get_object("places_list")  # a list store
                    self.custom_coords = []
                    if "experiment_positions" in self.conf_a["motion"]:
                        for key, val in self.conf_a["motion"]["experiment_positions"].items():
                            pl.append(["EXPERIMENT -- " + key])
                            self.custom_coords.append(val)

                    if "custom_positions" in self.conf_a["motion"]:
                        for key, val in self.conf_a["motion"]["custom_positions"].items():
                            pl.append([key])
                            self.custom_coords.append(val)

                    if len(self.custom_coords) == 0:
                        self.b.get_object("places_combo").set_visible(False)
                        self.b.get_object("places_label").set_visible(False)

                    # stage specific stuff
                    stage_parsed = urlparse(self.conf_a["motion"]["uri"])
                    stage_qparsed = parse_qs(stage_parsed.query)
                    if "el" in stage_qparsed:
                        splitted = stage_qparsed["el"][0].split(",")
                        esl = [float(y) for y in splitted]
                        if "spm" in stage_qparsed:
                            steps_per_mm = int(stage_qparsed["spm"][0])
                            length_oom = max([math.ceil(math.log10(x)) for x in esl])

                            movement_res = 1 / steps_per_mm
                            movement_res_oom = abs(math.floor(math.log10(movement_res)))
                            goto_field_width = length_oom + 1 + movement_res_oom

                            self.gotos = [self.b.get_object("goto_x"), self.b.get_object("goto_y"), self.b.get_object("goto_z")]
                            adjusters = [self.b.get_object("stage_x_adj"), self.b.get_object("stage_y_adj"), self.b.get_object("stage_z_adj")]
                            end_buffer_in_mm = 5  # don't allow the user to go less than this from the ends
                            for i, axlen in enumerate(esl):
                                self.gotos[i].set_width_chars(goto_field_width)
                                self.gotos[i].set_digits(movement_res_oom)
                                adjusters[i].set_value(axlen / 2)
                                adjusters[i].set_lower(end_buffer_in_mm)
                                adjusters[i].set_upper(axlen - end_buffer_in_mm)

                            # hide unused axes
                            self.num_axes = len(esl)
                            if self.num_axes < 3:
                                o = self.b.get_object("gtzl")
                                o.set_visible(False)
                                o = self.b.get_object("goto_z")
                                o.set_visible(False)
                            if self.num_axes < 2:
                                o = self.b.get_object("gtyl")
                                o.set_visible(False)
                                o = self.b.get_object("goto_y")
                                o.set_visible(False)
                        else:
                            self.lg.warning("Stage config URI malformed (couldn't find 'spm'). Stage disabled.")
                    else:
                        self.lg.warning("Stage config URI malformed (couldn't find 'el'). Stage disabled.")

                if self.enable_stage == False:
                    self.num_axes = 0
                    self.b.get_object("stage_util").set_visible(False)
                    self.b.get_object("estop_but").set_visible(False)
                else:
                    self.b.get_object("estop_but").set_visible(True)

                # smu type?
                try:
                    # TODO: handle multi smus here?
                    self.smu_type = self.conf_a["smus"][0]["type"]
                except:
                    self.smu_type = "k24xx"
                
                # hide the turbo button if it does nothing
                n_smus = len(self.conf_a["smus"])
                self.lg.debug(f"Found {n_smus} smu(s)")
                try:
                    if n_smus < 2:
                        self.b.get_object("turbo_mode").set_state(False)  # turn off turbo mode if we only have one smu
                        self.b.get_object("turbo_mode_box").set_visible(False)
                except Exception as e:
                    self.lg.debug(f"Exception {repr(e)}")

                if "UI" in self.conf_a:
                    ssa = self.b.get_object("sweep_start_adj")
                    sea = self.b.get_object("sweep_end_adj")
                    vdva = self.b.get_object("v_dwell_value_adj")
                    ila = self.b.get_object("i_lim_adj")
                    idva = self.b.get_object("i_dwell_value_adj")
                    ivsa = self.b.get_object("iv_steps_adj")
                    nplca = self.b.get_object("nplc_adj")  # TODO: consider list of valid NPLCs
                    sda = self.b.get_object("source_delay_adj")
                    if "v_max" in self.conf_a["UI"]:
                        ssa.set_upper(self.conf_a["UI"]["v_max"])
                        ssa.set_value(ssa.get_value())  # triger limits clamp
                        sea.set_upper(self.conf_a["UI"]["v_max"])
                        sea.set_value(sea.get_value())  # triger limits clamp
                        vdva.set_upper(self.conf_a["UI"]["v_max"])
                        vdva.set_value(vdva.get_value())  # triger limits clamp
                    if "v_min" in self.conf_a["UI"]:
                        ssa.set_lower(self.conf_a["UI"]["v_min"])
                        ssa.set_value(ssa.get_value())  # triger limits clamp
                        sea.set_lower(self.conf_a["UI"]["v_min"])
                        sea.set_value(sea.get_value())  # triger limits clamp
                        vdva.set_lower(self.conf_a["UI"]["v_min"])
                        vdva.set_value(vdva.get_value())  # triger limits clamp
                    if "i_max" in self.conf_a["UI"]:
                        ila.set_upper(self.conf_a["UI"]["i_max"] * 1000)
                        ila.set_value(ila.get_value())  # triger limits clamp
                        idva.set_upper(self.conf_a["UI"]["i_max"] * 1000)
                        idva.set_value(idva.get_value())  # triger limits clamp
                    if "i_min" in self.conf_a["UI"]:
                        ila.set_lower(self.conf_a["UI"]["i_min"] * 1000)
                        ila.set_value(ila.get_value())  # triger limits clamp
                        idva.set_lower(self.conf_a["UI"]["i_min"] * 1000)
                        idva.set_value(idva.get_value())  # triger limits clamp
                    if "nplc_min" in self.conf_a["UI"]:
                        nplca.set_lower(self.conf_a["UI"]["nplc_min"])
                        nplca.set_value(nplca.get_value())  # triger limits clamp
                    if "nplc_max" in self.conf_a["UI"]:
                        nplca.set_upper(self.conf_a["UI"]["nplc_max"])
                        nplca.set_value(nplca.get_value())  # triger limits clamp
                    if "npoints_min" in self.conf_a["UI"]:
                        ivsa.set_lower(self.conf_a["UI"]["npoints_min"])
                        ivsa.set_value(ivsa.get_value())  # triger limits clamp
                    if "npoints_max" in self.conf_a["UI"]:
                        ivsa.set_upper(self.conf_a["UI"]["npoints_max"])
                        ivsa.set_value(ivsa.get_value())  # triger limits clamp
                    if "delay_min" in self.conf_a["UI"]:
                        sda.set_lower(self.conf_a["UI"]["delay_min"] * 1000)
                        sda.set_value(sda.get_value())  # triger limits clamp
                    if "delay_max" in self.conf_a["UI"]:
                        sda.set_upper(self.conf_a["UI"]["delay_max"] * 1000)
                        sda.set_value(sda.get_value())  # triger limits clamp

                # # apply smu specific adjustments
                # if self.smu_type == "ark-metrica":
                #     if pversion.parse(self.conf_a["smus"][0]["version"]) <= pversion.parse("1.0.0"):
                #         self.b.get_object("i_dwell_setpoint_box").set_visible(False)
                #         self.b.get_object("current_limit_box").set_visible(False)
                #         self.b.get_object("auto_delay_box").set_visible(False)
                #         self.b.get_object("rrt_box").set_visible(False)
                #         self.b.get_object("ad_switch").set_active(False)
                #         self.b.get_object("invert_conn_box").set_visible(True)
                #         self.b.get_object("live_device_box").set_visible(True)
                #         self.b.get_object("sd_lab").set_sensitive(True)
                #         self.b.get_object("source_delay").set_sensitive(True)
                #         self.b.get_object("source_delay").set_visible(True)
                #         self.b.get_object("sd_dt").set_sensitive(True)
                #         self.b.get_object("i_dwell_check").set_label("Step 1: Steady-State Open-Circuit Voltage")
                #         self.b.get_object("i_dwell").set_tooltip_markup("Amount of time for V<sub>oc</sub> measurement")
                #         # self.b.get_object("integration_time_box").set_visible(False)
                #         self.b.get_object("iv_steps_adj").set_upper(10001)
                #         ssa = self.b.get_object("sweep_start_adj")
                #         self.b.get_object("sweep_start").set_value(0)
                #         ssa.set_upper(5)
                #         ssa.set_lower(0)
                #         ssa.set_value(0)
                #         sea = self.b.get_object("sweep_end_adj")
                #         sea.set_upper(5)
                #         sea.set_lower(0)
                #         sea.set_value(1.2)
                #         self.b.get_object("sweep_end").set_value(1.2)
                #         self.b.get_object("nplc").set_value(0.1)


                #         self.b.get_object("iv_steps_adj").set_upper(10001)

                #         # sweep param checks
                #         self.b.get_object("nplc").connect("value-changed", self.is_usb_smu_config_good)
                #         self.b.get_object("iv_steps").connect("value-changed", self.is_usb_smu_config_good)
                #         self.b.get_object("source_delay").connect("value-changed", self.is_usb_smu_config_good)

                # are we using a lockin here?
                try:
                    self.enable_lia = self.conf_a["lia"]["enabled"] == True
                except:
                    self.enable_lia = False

                # are we using a monochromator here?
                try:
                    self.enable_mono = self.conf_a["monochromator"]["enabled"] == True
                except:
                    self.enable_mono = False

                # are we using a SMU here?
                try:
                    self.enable_smu = any([smu["enabled"] == True for smu in self.conf_a["smus"]])
                except:
                    self.enable_smu = False

                # are we using a solar sim here?
                try:
                    self.enable_solarsim = self.conf_a["solarsim"]["enabled"] == True
                except:
                    self.enable_solarsim = False

                # are we using a bias light PSU here?
                try:
                    self.enable_psu = self.conf_a["psu"]["enabled"] == True
                except:
                    self.enable_psu = False

                # attempt service launching
                if sdb.ready == True:
                    for service in self.services_to_launch:
                        try:
                            sdb.start_service(service)
                        except Exception:
                            pass  # fail silently if we can't start a service
                else:
                    self.b.get_object("service_util").set_visible(False)
                    self.lg.warning("Not starting services.")

                if self.debug == True:
                    self.b.get_object("service_util").set_visible(True)
                    self.b.get_object("debug_box").set_visible(True)
                else:
                    self.b.get_object("service_util").set_visible(False)
                    self.b.get_object("debug_box").set_visible(False)

                if self.show_conn == True:
                    self.b.get_object("conn_btn").set_visible(True)
                else:
                    self.b.get_object("conn_btn").set_visible(False)

                if (self.show_dumb == True) and (self.debug == True):
                    self.b.get_object("dumb_flow").set_visible(True)
                else:
                    self.b.get_object("dumb_flow").set_visible(False)

                if self.show_sweep_deets == True:
                    self.b.get_object("sweep_deets_flow").set_visible(True)
                else:
                    self.b.get_object("sweep_deets_flow").set_visible(False)

                if self.show_analysis_tab == True:
                    self.b.get_object("analysis_tree").set_visible(True)
                else:
                    self.b.get_object("analysis_tree").set_visible(False)

                if self.restrict_nplc == True:
                    nplc_adj = self.b.get_object("nplc_adj")
                    nplc_adj.set_lower(1)
                    nplc_adj.set_step_increment(1)

                self.enable_iv = False
                if self.enable_smu:
                    self.enable_iv = True
                    # do some multismu checks
                    if len(self.conf_a["smus"]) > 1:
                        max_sublist = max([len(x) for x in self.conf_a["slots"]["group_order"]])
                        if max_sublist > len(self.conf_a["smus"]):
                            self.lg.warning("group_order dimensions do not match number of configured SMUs. Using only the first SMU.")
                            self.conf_a["smus"] = [self.conf_a["smus"][0]]  # delete all but the first

                    # if we passed the multismu config checks...
                    if len(self.conf_a["smus"]) > 1:
                        self.b.get_object("live_device_box").set_visible(True)

                # hide GUI elements that don't match what were configured to use
                self.b.get_object("eqe_frame").set_visible(self.enable_eqe)
                self.b.get_object("psu_cal_label").set_visible(self.enable_eqe)
                self.b.get_object("eqe_cal_label").set_visible(self.enable_eqe)

                if (self.enable_controller == False) and (self.enable_mux == False):
                    self.b.get_object("mux_toggle_box").set_visible(False)
                    self.b.get_object("rrt_box").set_visible(False)

                self.b.get_object("estop_but").set_visible(False)

                if self.enable_iv == False:
                    self.b.get_object("iv_frame").set_visible(False)
                    self.b.get_object("vt_wv").set_visible(False)
                    self.b.get_object("iv_wv").set_visible(False)
                    self.b.get_object("mppt_wv").set_visible(False)
                    self.b.get_object("jt_wv").set_visible(False)

                if self.enable_solarsim == False:
                    self.b.get_object("spectrum_box").set_visible(False)
                    self.b.get_object("ss_box").set_visible(False)
                    self.b.get_object("ss_int_box").set_visible(False)
                    self.b.get_object("ill_box").set_visible(False)
                    self.b.get_object("iv_cal_label").set_visible(False)
                    self.b.get_object("lit_sweep").set_active(3)  # if there's no solarsim assume only illuminated sweeps
                else:
                    # disable spectral stuff for non-wavelabs light source
                    try:
                        ss_kind = self.conf_a["solarsim"]["kind"]
                        if ss_kind != "wavelabs":
                            self.b.get_object("spectrum_box").set_visible(False)
                            self.b.get_object("ss_box").set_visible(False)
                            self.b.get_object("ss_int_box").set_visible(False)
                            self.b.get_object("iv_cal_label").set_visible(False)
                    except Exception as e:
                        pass

                if self.b.get_object("ss_int_box").get_visible() == False:
                    self.b.get_object("suns_voc_box").set_visible(False)

                # draw layouts
                angle = float(self.conf_a["UI"]["array_sketch"]["rotation"])
                for layout in self.conf_a["substrates"]["layouts"]:
                    if not layout["hidden"]:
                        self.layout_drawings[layout["name"]] = self.draw_layout(layout, angle)

                default_layouts, default_checkmarks, default_areas, default_pads = self.gen_defaults()

                self.reset_slot_config(default_layouts)

                # self.slot_config_store.connect('row-changed', self.on_slot_store_change)
                self.add_variable("Variable")

                # device selection stuff
                self.device_select_tv = self.b.get_object("device_tree")
                self.setup_device_select_tv(self.device_select_tv)

                # these treestores contain the info on which devices are selected for measurement
                # [str, bool, bool, str, bool] is for [label, checked, inconsistent, area, check visible]
                self.iv_store = Gtk.TreeStore(str, bool, bool, str, bool)
                self.iv_store.set_name("IV Device Store")

                self.fill_device_select_store(
                    store=self.iv_store,
                    checkmarks=default_checkmarks,
                    substrate_designators=[s["name"] for s in self.conf_a["slots"]["list"]],
                    labels=[""] * len(self.conf_a["slots"]["list"]),
                    layouts=default_layouts,
                    areas=default_areas,
                    pads=default_pads,
                )

                max_devices = len([m for g in self.conf_a["slots"]["group_order"] for m in g])
                max_address_string_length = math.ceil(max_devices / 4)
                selection_box_length = max_address_string_length + 2

                self.iv_dev_box = self.b.get_object("iv_devs")
                sc = self.iv_dev_box.get_style_context()
                sc.add_class(Gtk.STYLE_CLASS_MONOSPACE)
                self.iv_dev_box.set_width_chars(selection_box_length)
                self.iv_dev_box.connect("changed", self.update_measure_count)

                # self.eqe_dev_box = self.b.get_object("eqe_devs")
                # sc = self.eqe_dev_box.get_style_context()
                # sc.add_class(Gtk.STYLE_CLASS_MONOSPACE)
                # self.eqe_dev_box.set_width_chars(selection_box_length)
                # self.eqe_dev_box.connect("changed", self.update_measure_count)

                self.do_dev_store_update_tasks(self.iv_store)

                self.analysis_tv = self.b.get_object("analysis_tree")
                if self.show_analysis_tab == True:
                    self.analysis_store = self.setup_analysis_tv(self.analysis_tv)
                    self.analysis_tv.set_model(self.analysis_store)

                # the device picker popover
                self.po = self.b.get_object("picker_po")
                self.po.set_position(Gtk.PositionType.BOTTOM)

                # the layout popover
                self.lopo = self.b.get_object("layout_po")
                self.lopo.set_position(Gtk.PositionType.RIGHT)
                self.lopo.set_relative_to(self.slot_config_tv)
                # self.lopo.add(Gtk.Image())
                # self.lopo.add(Gtk.Label(label=layouts[0]))

                # for approximating runtimes
                # self.approx_seconds_per_iv = 50
                # self.approx_seconds_per_eqe = 150

                self.wvids = []
                self.wvids.append("vt_wv")
                self.wvids.append("iv_wv")
                self.wvids.append("mppt_wv")
                self.wvids.append("jt_wv")
                # self.wvids.append("eqe_wv")

                self.uris = []  # the uris to put in the webviews
                if "network" in self.conf_a:
                    if "live_data_uris" in self.conf_a["network"]:
                        for uri in self.conf_a["network"]["live_data_uris"]:
                            self.uris.append(uri)

                # setup MQTT client
                client_id = f"{self.get_property('application-id')}.{uuid.uuid4().hex}"
                self.lg.debug(f"Using MQTT client ID: {client_id}")
                self.mqttc = mqtt.Client(client_id=client_id)
                self.mqttc.on_message = self.on_mqtt_message
                self.mqttc.on_connect = self.on_mqtt_connect
                self.mqttc.on_disconnect = self.on_mqtt_disconnect

                # thread that manages the mqtt client connection
                self.mqtt_thread = threading.Thread(target=self.connect_mqtt_client, args=(self.mqttc,), daemon=True)
                self.mqtt_thread.start()

                # read the default recipe from the config and set the gui box to that
                if "solarsim" in self.conf_a:
                    if "recipes" in self.conf_a["solarsim"]:
                        tb = self.b.get_object("light_recipe")  # the active (editable combox box item)
                        tb.set_text(self.conf_a["solarsim"]["recipes"][0])
                        tbc = self.b.get_object("light_recipe_combo")
                        for recipe in self.conf_a["solarsim"]["recipes"]:
                            tbc.append_text(recipe)

                # read the default recipe from the config and set the gui box to that
                if "mppt" in self.conf_a:
                    if "presets" in self.conf_a["mppt"]:
                        tb = self.b.get_object("mppt_params")
                        tb.set_text(self.conf_a["mppt"]["presets"][0])
                        tbc = self.b.get_object("mppt_params_combo")
                        for recipe in self.conf_a["mppt"]["presets"]:
                            tbc.append_text(recipe)

                # read the invert plot settings from the config and set the switches to that
                if "plots" in self.conf_a:
                    if "invert_voltage" in self.conf_a["UI"]:
                        sw = self.b.get_object("inv_v_switch")
                        sw.set_active(self.conf_a["plots"]["invert_voltage"])
                    if "invert_current" in self.conf_a["UI"]:
                        sw = self.b.get_object("inv_i_switch")
                        sw.set_active(self.conf_a["plots"]["invert_current"])

                # make sure the plotter is in sync with us when we start
                self.on_plotter_switch(None, True)
                self.on_voltage_switch(None, self.b.get_object("inv_v_switch").get_active())
                self.on_current_switch(None, self.b.get_object("inv_i_switch").get_active())

                # connect the slot load button click event
                slb = self.b.get_object("slot_load_but")
                slb.connect("clicked", self.on_slot_load_but)

                # connect livechart button
                self.b.get_object("livechart_btn").connect("clicked", self.launch_livechart)

                # set bias led spinbox limits
                if "psu" in self.conf_a:
                    if "ch1_ocp" in self.conf_a["psu"]:
                        c1bla = self.b.get_object("ch1_bias_light_adj")
                        c1bla.set_upper(self.conf_a["psu"]["ch1_ocp"] * 1000)
                    if "ch2_ocp" in self.conf_a["psu"]:
                        c2bla = self.b.get_object("ch2_bias_light_adj")
                        c2bla.set_upper(self.conf_a["psu"]["ch2_ocp"] * 1000)
                    if "ch3_ocp" in self.conf_a["psu"]:
                        c3bla = self.b.get_object("ch3_bias_light_adj")
                        c3bla.set_upper(self.conf_a["psu"]["ch3_ocp"] * 1000)

                # for doing tasks when the user makes a stack change
                ms = self.b.get_object("stack")
                ms.props.visible_child_name = "setup"
                ms.connect("notify::visible-child", self.on_stack_change)

                # for handling global accelerator key combos
                ag = self.b.get_object("global_keystrokes")

                # setup service restart combo
                ag.connect(Gdk.keyval_from_name("R"), Gdk.ModifierType.SHIFT_MASK | Gdk.ModifierType.CONTROL_MASK, 0, self.do_restart_services)

                # setup debug key combo
                ag.connect(Gdk.keyval_from_name("D"), Gdk.ModifierType.CONTROL_MASK, 0, self.do_debug_tasks)

                # recalibrate stage keystroke
                ag.connect(Gdk.keyval_from_name("R"), Gdk.ModifierType.CONTROL_MASK, 0, self.force_home_stage)

                # disallow bad input for some boxes
                rnp = self.b.get_object("run_name_prefix")
                rnp.connect("insert-text", self.only_alnum)
                un = self.b.get_object("user_name")
                un.connect("insert-text", self.only_alnum)

                # do one tick now and then start the backround tick launcher
                self.tick()
                self.ticker_id = GLib.timeout_add_seconds(1, self.tick, None)
                self.b.connect_signals(self)  # maps all ui callbacks to functions here

                # handle live device change
                self.b.get_object("live_device_combo").connect("changed", self.on_live_device_changed)

                # enable the load_last button if we can find the latest autosave
                self.find_last()
            # except RuntimeError:
            #    raise
            # end of big try block that can catch any error associated with a bad config
            except Exception as e:  # TODO: consider wrapping a lot more in this try to ensure the gui dosn't crash on bad config
                self.lg.error(f"Failed to build GUI: {repr(e)}")
                # raise RuntimeError(f"Failure loading main configuration: {repr(e)}")

            self.main_win = self.b.get_object("mainWindow")
            self.main_win.set_application(self)
            self.main_win.connect("configure-event", self.handle_window_configure)

        self.main_win.present()

    def reset_slot_config(self, layouts):
        """makes/remakes/resets slot config treeview"""
        assert self.conf_a, "Config not properly loaded."
        slot_names = [s["name"] for s in self.conf_a["slots"]["list"]]

        # slot configuration stuff
        self.slot_config_tv = self.b.get_object("substrate_tree")
        self.slot_table_base_headings = self.setup_slot_config_tv(self.slot_config_tv, [item["name"] for item in self.conf_a["substrates"]["layouts"] if not item["hidden"]])
        self.slot_config_store = Gtk.ListStore(str, str, str)  # Slot, Label, Layout
        self.variables.clear()
        self.slot_config_tv.set_model(self.slot_config_store)
        self.fill_slot_config_store(
            self.slot_config_store,
            slot_names,
            [""] * len(slot_names),
            layouts,
        )

    def gen_defaults(self):
        """pick sane defaults"""
        assert self.conf_a, "Config not properly loaded."
        default_layouts = []
        default_checkmarks = []
        default_areas = []
        default_pads = []
        for slot_name in [s["name"] for s in self.conf_a["slots"]["list"]]:
            for layout in self.conf_a["substrates"]["layouts"]:
                if not layout["hidden"]:
                    npads = len(layout["pads"])
                    max_pads = len([m for g in self.conf_a["slots"]["group_order"] for m in g if m[0] == slot_name])
                    if npads <= max_pads:
                        default_layouts.append(layout["name"])
                        if "EXT" in slot_name:
                            default_checkmarks.append([False] * npads)
                        else:
                            default_checkmarks.append([True] * npads)
                        default_areas.append(layout["areas"])
                        default_pads.append(layout["pads"])
                        break
        return (default_layouts, default_checkmarks, default_areas, default_pads)

    # take a mesh grid and turn it into a list
    def grid_to_list(self, grid):
        lis = []
        fl = grid.flat
        for x in fl:
            lis.append(x)
        return lis

    def launch_livechart(self, button):
        """launches the external livechart tool"""
        GLib.spawn_command_line_async("livechart")

    # cleans up spectrum dialog window
    def on_spec_dialog_finish(self, dialog, *args, **kwargs):
        dialog.destroy()
        self.spectrum_plot_handle = None

    # handles rendering of spectrum plot
    def on_spec_plot_draw(self, drawing_area, cairo_context):
        if self.spectrum_plot_handle is None:
            drawing_area.queue_draw()
        else:
            self.spectrum_plot_handle.render_cairo(cairo_context)

    # takes the spectrum data we got from MQTT and sets it up for plotting
    def make_spec_svgh(self, spec):
        x = spec[0]
        y = spec[1]
        fig, ax = plt.subplots()  # Create a figure and an axes.
        fig.subplots_adjust(bottom=0)
        fig.subplots_adjust(top=1)
        fig.subplots_adjust(right=1)
        fig.subplots_adjust(left=0)
        ax.plot(x, y, label="Raw Data")  # Plot some data on the axes.
        ax.set_xlabel("Wavelength [nm]")  # Add an x-label to the axes.
        ax.set_ylabel("Counts")  # Add a y-label to the axes.
        ax.grid(True, which="both")
        # ax.set_title("Solar Sim Spectrum")  # Add a title to the axes.

        # size = fig.get_size_inches()*fig.dpi

        b = BytesIO()
        # fig.savefig('out.png', bbox_inches='tight',transparent=True, pad_inches=0
        # fig.tight_layout()
        size = fig.get_size_inches() * fig.dpi
        fig.savefig(b, format="svg", transparent=False, bbox_inches="tight", dpi="figure", pad_inches=0)
        svg_bytes = b.getvalue()

        # make a dialog box to hold the plot
        dialog_setup = {}
        dialog_setup["title"] = "Solar Sim Spectrum"
        dialog_setup["parent"] = self.main_win
        dialog_setup["destroy_with_parent"] = True
        dialog_setup["modal"] = True
        d = Gtk.Dialog(**dialog_setup)
        d.add_button(Gtk.STOCK_OK, Gtk.ResponseType.OK)
        d.connect("close", self.on_spec_dialog_finish)

        box = d.get_content_area()
        box.set_border_width(15)

        aa = d.get_action_area()
        aa.set_border_width(0)
        spec_plot_drawing = Gtk.DrawingArea()
        spec_plot_drawing.connect("draw", self.on_spec_plot_draw)

        svgh = Rsvg.Handle.new_from_data(svg_bytes)
        self.spectrum_plot_handle = svgh

        vb = svgh.get_intrinsic_dimensions()

        spec_plot_drawing.props.width_request = size[0]
        spec_plot_drawing.props.height_request = size[1]

        box.add(spec_plot_drawing)
        d.connect("response", self.on_spec_dialog_finish)
        d.show_all()

    def update_progress(self, progress_msg):
        pgb = self.b.get_object("progress")
        frac = progress_msg["fraction"]
        if frac == 1:
            pgb.set_visible(False)
        else:
            pgb.set_visible(True)
            pgb.set_text(progress_msg["text"])
            pgb.set_fraction(frac)

    def on_mqtt_connect(self, client, userdata, flags, rc):
        self.lg.debug(f"Broker connected with result code {rc}")
        GLib.idle_add(self.hb.set_subtitle, f"Status: Connected | {self.run_handler_status}")

        # subscribe to cli topic to report back on progress
        # self.mqttc.subscribe("cli/#", qos=2)

        # log messages from the measurement backend
        client.subscribe("measurement/log/#", qos=2)

        # log messages from the calibration backend
        client.subscribe("calibration/#", qos=2)

        # a channel for results from completed commands
        client.subscribe("response/#", qos=2)

        # a channel for progress messages
        client.subscribe("progress/#", qos=2)

        # a channel for keeping track of devices currently under measurement
        client.subscribe("plotter/live_devices/#", qos=2)

        # a channel for progress messages
        client.subscribe("status/#", qos=2)

        # what state the measurement backend is in
        client.subscribe("measurement/status/#", qos=2)

    def on_mqtt_disconnect(self, client, userdata, rc):
        if self.terminating == False:
            self.lg.warning(f"Broker disconnected with result code {rc}")
            GLib.idle_add(self.hb.set_subtitle, "Status: Disconnected")
            self.run_handler_status = "Unknown"
            self.b.get_object("run_but").set_sensitive(False)  # instantly gray run button on broker disconnect

    def on_mqtt_message(self, client, userdata, msg):
        """Act on an MQTT message."""
        try:
            m = json.loads(msg.payload)
        except:
            m = None

        # examine by message topic
        if m is not None:
            if (msg.topic) == "measurement/status":
                self.run_handler_status = m
                GLib.idle_add(self.hb.set_subtitle, f"Status: Connected | {self.run_handler_status}")
                if m == "Ready":
                    GLib.idle_add(self.b.get_object("run_but").set_sensitive, True)
                    # self.b.get_object("run_but").set_sensitive(True)
                if (m == "Offline") or (m == "Busy"):
                    GLib.idle_add(self.b.get_object("run_but").set_sensitive, False)
                    # self.b.get_object("run_but").set_sensitive(False)  # prevent multipress
            elif (msg.topic) == "measurement/log":
                self.lg.log(m["level"], m["msg"])
            elif (msg.topic) == "progress":  # handle progress bar message
                GLib.idle_add(self.update_progress, m)  # this can't be done in the MQTT thread. do it in idle later
            elif (msg.topic) == "plotter/live_devices":
                GLib.idle_add(self.update_live_devices, m)  # this can't be done in the MQTT thread. do it in idle later
            elif (msg.topic) == "calibration/spectrum":
                self.iv_cal_time = m["timestamp"]
                if self.want_spectrum == True:
                    self.want_spectrum = False
                    spec = m["data"]
                    # better to do the rest in GLib loop idle, not in the MQTT thread
                    GLib.idle_add(self.make_spec_svgh, spec)  # this can't be done in the MQTT thread. do it in idle later

            elif "calibration/psu" in msg.topic:
                self.psu_cal_time = m["timestamp"]

            elif msg.topic == "status":
                if "warn_dialog" in m:
                    # self.connection_warning_dialog(m["warn_dialog"]["headline"], m["warn_dialog"]["body"], m["warn_dialog"]["buttons"])
                    GLib.idle_add(self.connection_warning_dialog, m["warn_dialog"]["headline"], m["warn_dialog"]["body"], m["warn_dialog"]["buttons"])

            # examine by message content
            # if "log" in m:  # log update message
            #    self.lg.log(m["log"]["level"], m["log"]["text"])

            if "pos" in m:  # position update message
                pos = m["pos"]
                if len(pos) != self.num_axes:
                    self.lg.warning(f"Stage dimension mismatch")
                else:
                    for ax, val in pos.items():
                        axi = int(ax) - 1
                        try:
                            self.gotos[axi].set_value(val)
                        except:
                            self.gotos[axi].set_text("")
                            self.lg.warning(f"Failed to read axis {ax} position")

    def connect_mqtt_client(self, mqttc):
        while self.terminating == False:
            GLib.idle_add(self.hb.set_subtitle, "Status: Connecting")
            try:
                mqttc.connect(self.mqtt_server)

                mqttc.loop_forever(retry_first_connection=True)
            except:
                self.lg.error("Unable to connect to the backend.")
                time.sleep(10)  # chill on the reconnect attempts

    # updates the list of devices currently under measurement
    def update_live_devices(self, new_labels):
        ldc = self.b.get_object("live_device_combo")
        if len(new_labels) == 0:
            ldc.remove_all()
            ldc.set_active(-1)
            ldc.set_sensitive(False)
        else:
            if len(new_labels) == 1:
                ldc.set_sensitive(False)
            else:
                ldc.set_sensitive(True)
            previously_selected = ldc.get_active_text()
            new_set = set(new_labels)
            model = ldc.get_model()
            old_labels = [x[0] for x in model]
            old_set = set(old_labels)
            to_add = new_set.difference(old_set)
            to_remove = old_set.difference(new_set)

            if len(to_remove) > 0:
                i_to_remove = [old_labels.index(x) for x in to_remove]
                i_to_remove_rev = i_to_remove[::-1]
                for remove_i in i_to_remove_rev:
                    ldc.remove(remove_i)

            for new_one in to_add:
                ldc.append_text(new_one)

            # keep the previously selected device selected
            if previously_selected in new_labels:
                current_labels = [x[0] for x in model]
                new_id = current_labels.index(previously_selected)
                ldc.set_active(new_id)
            else:
                ldc.set_active(0)

    # tells the plotter to filter data from devices that are not the one in the combo box
    def on_live_device_changed(self, combo):
        msg = json.dumps(combo.get_active_text())
        self.emit_message("plotter/live_device", payload=msg, qos=2)

    def update_measure_count(self, entry):
        parent_frame = entry.get_parent().get_parent()
        text_is = entry.get_text()
        selection_bitmask = int(text_is, 16)
        num_selected = sum([c == "1" for c in bin(selection_bitmask)])
        txt = f"Device Selection Bitmask ({num_selected} selected)"
        parent_frame.set_label(txt)

    # this configures a treeview object for use in selecting/unselecing devices.
    def setup_device_select_tv(self, tree_view):
        if len(tree_view.get_columns()) == 0:
            renderDesignator = Gtk.CellRendererText()
            # the first column is created
            designator = Gtk.TreeViewColumn("Substrate/Device", renderDesignator, text=0)
            tree_view.append_column(designator)

        if len(tree_view.get_columns()) == 1:
            renderDesignator = Gtk.CellRendererText()
            # the first column is created
            designator = Gtk.TreeViewColumn("Area/Layout", renderDesignator, markup=3)
            tree_view.append_column(designator)

        # the cellrenderer for the second column - boolean rendered as a toggle
        if len(tree_view.get_columns()) == 2:
            renderCheck = Gtk.CellRendererToggle()
            # the second column is created
            colCheck = Gtk.TreeViewColumn("Measure?", renderCheck, active=1, inconsistent=2, visible=4)
            # colCheck.set_clickable(True)
            # colCheck.connect("clicked", self.dev_col_click)
            tree_view.append_column(colCheck)
            # connect the cellrenderertoggle with a callback function
            renderCheck.connect("toggled", self.on_dev_toggle, colCheck)

        # this handles left/right arrow buttons for expanding and collapsing rows
        tree_view.connect("key-release-event", self.handle_dev_key)

    # populates a device selection store with the default startup values
    # checkmarks is a list of lists of booleans
    # len(checkmarks) is the number of substrates
    # and the lengths of the inner lists are the number of pixels on each substrate
    def fill_device_select_store(self, store, checkmarks, substrate_designators, labels, layouts, areas, pads):
        store.clear()
        # compute the checkbox state for the top level
        any_check = False
        all_check = True
        for subs_bool in checkmarks:
            if all(subs_bool):
                any_check = True
            else:
                all_check = False
                if any(subs_bool):
                    any_check = True
        if all_check == True:
            # [str, bool, bool, str, bool] is for [label, checked, inconsistent, area, check visible]
            checked = True
            inconsistent = False
        else:
            checked = False
            if any_check == True:
                inconsistent = True
            else:
                inconsistent = False
        total_devices = 0
        for subs_areas in areas:
            total_devices += len(subs_areas)
        if total_devices == 0:
            top_vis = False
        else:
            top_vis = True
        top = store.append(None, ["All", checked, inconsistent, "", top_vis])

        # now we can fill in the model
        for i, subs_checks in enumerate(checkmarks):  # iterate through substrates
            this_label = substrate_designators[i]
            subs_areas = areas[i]
            subs_pads = pads[i]
            if labels[i] != "":
                this_label += f": {labels[i]}"
            if all(subs_checks):
                checked = True
                inconsistent = False
            else:
                checked = False
                if any(subs_checks):
                    inconsistent = True
                else:
                    inconsistent = False
            # append a substrate header row
            piter = store.append(top, [this_label, checked, inconsistent, layouts[i], len(subs_areas) != 0])
            for checked, area, pad in zip(subs_checks, subs_areas, subs_pads):
                # append a device row
                store.append(piter, [f"Device {pad}", checked, False, f"{area:,.3f} cm<sup>2</sup>", True])

    def setup_analysis_tv(self, tree_view):
        """Sets up the cols for the analysis table"""

        cols = []
        cols.append({"name": "Slot", "typ": str})
        cols.append({"name": "Label", "typ": str})
        cols.append({"name": "#", "typ": str})
        cols.append({"name": "Area [cm^2]", "typ": str})
        cols.append({"name": "V_oc [mA*cm^-2]", "typ": str})
        cols.append({"name": "J_sc [mV]", "typ": str})
        cols.append({"name": "P_max [mW*cm^-2]", "typ": str})
        cols.append({"name": "FF [%]", "typ": str})

        types = []

        for i, col in enumerate(cols):
            cell = Gtk.CellRendererText()
            tvc = Gtk.TreeViewColumn(col["name"], cell, text=i)
            tree_view.append_column(tvc)
            types.append(col["typ"])

        return Gtk.ListStore(*tuple(types))

    # sets up the columns the slot configuration treeview will show us
    def setup_slot_config_tv(self, tree_view: Gtk.TreeView, layout_names: list[str]):
        col_headings = []
        # clear the cols
        for col in tree_view.get_columns():
            tree_view.remove_column(col)

        # the refrence designator column
        new_col_num = 0
        new_col_name = "Slot"
        col_headings.append(new_col_name)
        ref_des_cell = Gtk.CellRendererText()
        ref_des_col = Gtk.TreeViewColumn(new_col_name, ref_des_cell, text=new_col_num)
        tree_view.append_column(ref_des_col)

        # the editable substrate label column
        # only append the col if it's not already there
        # fixes double col on file load
        new_col_num = 1
        new_col_name = "Label"
        col_headings.append(new_col_name)
        label_cell = Gtk.CellRendererText()
        label_cell.set_property("editable", True)
        label_cell.connect("edited", self.on_slot_cell_edit, (new_col_num, False, True))
        labels_col = Gtk.TreeViewColumn(new_col_name, label_cell, text=new_col_num)
        tree_view.append_column(labels_col)

        # the layout dropdown selection column
        new_col_num = 2
        new_col_name = "Layout"
        col_headings.append(new_col_name)
        # this allows me to find the combo box object
        tree_view.connect("set-focus-child", self.on_layout_combo_focus)
        layout_cell = Gtk.CellRendererCombo()
        layouts_store = Gtk.ListStore(str)  # holds the layout options
        for name in layout_names:
            layouts_store.append([name])
        layout_cell.set_property("editable", True)
        layout_cell.set_property("model", layouts_store)
        layout_cell.set_property("has-entry", False)
        layout_cell.set_property("text-column", 0)
        layout_cell.connect("changed", self.on_layout_combo_changed)
        layout_col = Gtk.TreeViewColumn(new_col_name, layout_cell, text=new_col_num)
        tree_view.append_column(layout_col)

        # return the names of the cols used here
        return col_headings

    def on_col_header_click(self, col, store_col):
        d = Gtk.MessageDialog(parent=self.main_win, buttons=Gtk.ButtonsType.CANCEL)
        d.set_property("text", "<b>Variable Management</b>")
        d.set_property("message-type", Gtk.MessageType.QUESTION)
        d.set_property("use-markup", True)
        delete_code = 4
        if (store_col != 3) and (store_col == (len(self.variables) + 2)):  # the user can only delete the last var col if it's not the only one
            d.add_button("Delete", delete_code)
        rename_code = 5
        d.add_button("Rename", rename_code)
        new_code = 6
        d.add_button("Add New", new_code)

        hb = Gtk.HBox()
        hb.add(Gtk.Label(label="New Variable Name: "))
        var_entry = Gtk.Entry()
        pre_pop_txt = col.get_title()
        if pre_pop_txt == "":
            pre_pop_txt = "Variable"
        var_entry.set_text(pre_pop_txt)
        var_entry.connect("changed", self.check_empty_editable, (d, (rename_code, new_code)))  # check empty variable
        var_entry.connect("insert-text", self.only_alnum)  # prevent strange var names
        hb.add(var_entry)
        mdb = d.get_message_area()
        mdb.add(hb)
        mdb.show_all()
        response = d.run()
        box_text = var_entry.get_text()
        d.destroy()
        if response != Gtk.ResponseType.CANCEL:
            if response == delete_code:
                self.slot_config_tv.remove_column(col)
                self.delete_variable()
            elif response == rename_code:
                col.set_title(box_text)
                self.variables[store_col - 3] = box_text
            elif response == new_code:
                self.add_variable(box_text)

    def check_empty_editable(self, entry_widget, user_data):
        if len(entry_widget.get_text()) == 0:
            good = False
        else:
            good = True
        for code in user_data[1]:
            user_data[0].set_response_sensitive(code, good)

    # deletes the last variable
    # can only delete the last one because deleting
    # any one besides that one can mess up all the existing col # refs
    # and fixing that seems hard
    # assumes the treeview col as already been deleted
    # updates the slot config store to remove the variable's data
    def delete_variable(self):
        del self.variables[-1]
        # we must create the store in the proper shape
        create_params = tuple([str] * (len(list(self.slot_config_store[0])) - 1))
        new_store = Gtk.ListStore(*create_params)  # ref des, user label, layout name, then the variable cols
        for row in self.slot_config_store:
            new_row = list(row)
            del new_row[-1]
            new_store.append(tuple(new_row))
        self.slot_config_tv.set_model(new_store)
        del self.slot_config_store
        self.slot_config_store = new_store
        # self.slot_config_store.connect('row-changed', self.on_slot_store_change)

    # registers a new variable
    # adds the variable name to the variables list
    # inserts a col for it in the slot config treeview
    # and if add_store_col == True, appends a new col for it in the slot config liststore
    def add_variable(self, var_name):
        self.variables.append(var_name)
        store_col = 2 + len(self.variables)

        # we must create the store in the proper shape
        create_params = tuple([str] * (len(list(self.slot_config_store[0])) + 1))
        new_store = Gtk.ListStore(*create_params)  # ref des, user label, layout name, then the variable cols
        for row in self.slot_config_store:
            new_row = list(row) + [""]
            new_store.append(tuple(new_row))
        self.slot_config_tv.set_model(new_store)
        del self.slot_config_store
        self.slot_config_store = new_store
        # self.slot_config_store.connect('row-changed', self.on_slot_store_change)

        var_cell = Gtk.CellRendererText()
        var_cell.set_property("editable", True)
        var_cell.connect("edited", self.on_slot_cell_edit, (store_col, False, True))
        var_col = Gtk.TreeViewColumn(var_name, var_cell, text=store_col)
        var_col.set_clickable(True)
        var_col.connect("clicked", self.on_col_header_click, store_col)
        self.slot_config_tv.insert_column(var_col, store_col)

    def on_new_var_button(self, button):
        self.add_variable(self.b.get_object("new_var").get_text())

    # populates the slot config store with the default startup values
    def fill_slot_config_store(self, store, substrate_designators, labels, layouts):
        store.clear()
        for i in range(len(substrate_designators)):
            store.append([substrate_designators[i], labels[i], layouts[i]])

    # the user chose a new layout. save that choice in the slot config store
    def on_layout_combo_changed(self, widget, path, ti):
        self.array_drawing_handle = None  # invalidate drawing handle to force redraw
        store_col = 2
        new_layout = widget.props.model[ti][0]
        self.slot_config_store[path][store_col] = new_layout
        GLib.idle_add(self.on_slot_store_change, self.slot_config_store, path, self.slot_config_store.get_iter(path), (store_col, True))

    # the user has hovered their mouse over a layout choice
    def on_layout_combo_entered(self, widget, event, layout_name):
        d = self.layout_drawings[layout_name][1]
        if isinstance(svg_str := d.as_svg(), str):
            svgh = Rsvg.Handle.new_from_data(svg_str.encode())
            self.layout_drawing_handle = svgh
            vb = svgh.get_intrinsic_dimensions()

            self.subs_pic.props.width_request = vb.out_width.length
            self.subs_pic.props.height_request = vb.out_height.length
            self.subs_pic.queue_draw()
            self.lopo.popup()
            self.lopo.show_all()

    # called when the substrate (lightmask) picture is being drawn
    def on_subs_pic_draw(self, drawing_area, cairo_context):
        if self.layout_drawing_handle is None:
            drawing_area.queue_draw()
        else:
            rh = self.layout_drawing_handle
            rh.render_cairo(cairo_context)

    # the layout ComboBox has now been magically created!
    # so let's install our hover-focus callbacks into its menu widget children
    def on_layout_combo_focus(self, treeview, combobox):
        try:
            liststore = combobox.get_model()
            popup_menu_widget = combobox.get_popup_accessible().props.widget
            popup_menu_widget.connect("unmap", lambda x: self.lopo.popdown())
            for i, c in enumerate(popup_menu_widget.get_children()):
                layout_name = liststore[i][0]
                c.connect("enter-notify-event", self.on_layout_combo_entered, layout_name)
        except:
            pass

    # the user has made a text edit in the slot config table
    def on_slot_cell_edit(self, widget, path, text, user_data):
        col = user_data[0]
        immediate_slot_store_change = user_data[1]
        do_dev_store_update_tasks = user_data[2]
        if col == 1:  # only sanitize col #1 (label)
            valid = re.compile("[^a-zA-Z0-9_\-\.]+", re.UNICODE)  # type:ignore
            fill = valid.sub("", text)
        else:
            fill = text
        self.slot_config_store[path][col] = fill
        if immediate_slot_store_change:
            self.on_slot_store_change(self.slot_config_store, path, self.slot_config_store.get_iter(path), (col, do_dev_store_update_tasks))
        else:
            GLib.idle_add(self.on_slot_store_change, self.slot_config_store, path, self.slot_config_store.get_iter(path), (col, do_dev_store_update_tasks))

    # called when a user pushes a device selection toggle button
    # updates the store ticked and inconsistent values
    # for this row and all of its children and grandchildren
    # does not touch parents
    def on_dev_toggle(self, toggle, path, tree_col):
        store = tree_col.get_tree_view().get_model()
        checked = not toggle.get_active()
        store[path][1] = checked
        store[path][2] = False  # can't be inconsistent
        GLib.idle_add(self.calc_checkboxes, path, store, checked)

    def calc_checkboxes(self, path, store, checked):
        # make this selection flow down to all children and grandchildren
        titer = store.get_iter_from_string(str(path))  # this row's iterator
        citer = store.iter_children(titer)
        while citer is not None:
            store.set_value(citer, 1, checked)
            store.set_value(citer, 2, False)  # can't be inconsistent
            gciter = store.iter_children(citer)
            while gciter is not None:
                store.set_value(gciter, 1, checked)
                store.set_value(gciter, 2, False)  # can't be inconsistent
                gciter = store.iter_next(gciter)
            citer = store.iter_next(citer)
        GLib.idle_add(self.do_dev_store_update_tasks, store)
        # self.do_dev_store_update_tasks(store)

    # handles keystroke in the label creation tree
    # def handle_label_key(self, tv, event):
    #     keyname = Gdk.keyval_name(event.keyval)
    #     if keyname in ["Return", "Enter"]:
    #         path, col = self.label_tree.get_cursor()
    #         path.next()
    #         self.label_tree.set_cursor_on_cell(
    #             path, focus_column=col, focus_cell=None, start_editing=True
    #         )

    # handles keystroke in the device selection tree
    def handle_dev_key(self, tv, event):
        # eqe = 'eqe' in Gtk.Buildable.get_name(self.po.get_relative_to())
        keyname = Gdk.keyval_name(event.keyval)
        if keyname in ["Right", "Left"]:
            path, col = tv.get_cursor()
            if tv.row_expanded(path) is True:
                tv.collapse_row(path)
            else:
                tv.expand_row(path)

    # handle the auto iv toggle
    def on_autoiv_toggled(self, button, user_data=None):
        siblings = button.get_parent().get_children()
        siblings.remove(button)
        ali = siblings[0]

        if button.get_active():
            sensitivity = False
        else:
            sensitivity = True

        ali.set_sensitive = sensitivity
        for child in ali.get_children():
            child.set_sensitive(sensitivity)
            for grandchild in child.get_children():
                grandchild.set_sensitive(sensitivity)

    # terminates the app
    def terminate(self, user_data=None):
        self.lg.debug("terminate called")
        self.quit()

    # runs once per second
    def tick(self, user_data=None):
        if self.iv_cal_time is None:
            human_dt = "No record"
        else:
            human_dt = humanize.naturaltime(dt.datetime.now() - dt.datetime.fromtimestamp(self.iv_cal_time))
        self.b.get_object("iv_cal_label").set_text(f"Solar Sim Intensity Cal. Age: {human_dt}")
        rns = self.b.get_object("run_name_suffix")
        now = int(time.time())
        rns.set_text(str(now))
        return True

    # a function that blocks non-alphanumeric text entry
    def only_alnum(self, widget, text, text_len, text_pos):
        if text_len > 0:
            allowed = "_-"
            if text.isalnum():
                return True
            elif text in allowed:
                return True
            else:
                widget.stop_emission_by_name("insert-text")

    # gets called on changes to text boxes that impact the run name
    def update_run_name(self, widget):
        un = self.b.get_object("user_name")
        unt = un.get_text()
        rnp = self.b.get_object("run_name_prefix")
        rnpt = rnp.get_text()
        rns = self.b.get_object("run_name_suffix")
        rnst = rns.get_text()

        if unt != "":
            run_name = f"{unt}{os.sep}"
        else:
            run_name = f"unknown_user{os.sep}"
        run_name += rnpt
        run_name += rnst
        rn = self.b.get_object("run_name")
        rn.set_text(run_name)

    # draws the whole slot array
    def draw_array(self):
        assert self.conf_a, "Config not properly loaded."
        max_render_pix = 600  # the picture's largest dim will be this many pixels

        x_cens = [item["offset"][0] for item in self.conf_a["slots"]["list"] if item["offset"][0] is not None]
        y_cens = [item["offset"][1] for item in self.conf_a["slots"]["list"] if item["offset"][1] is not None]

        angle = self.conf_a["UI"]["array_sketch"]["rotation"]
        if self.conf_a["UI"]["array_sketch"]["mirror"][0]:
            scalex = -1
        else:
            scalex = 1
        if self.conf_a["UI"]["array_sketch"]["mirror"][1]:
            scaley = -1
        else:
            scaley = 1
        rot = f"scale({scalex},{scaley}) rotate({angle},0,0)"
        rg = drawsvg.Group(**{"transform": rot})

        big_font_size = max_render_pix / 75
        x_sizes = [big_font_size]
        y_sizes = [big_font_size]

        for i, row in enumerate(self.slot_config_store):
            slot_name = row[0]
            layout_name = row[2]
            layout = next(item for item in self.conf_a["substrates"]["layouts"] if item["name"] == layout_name)
            size = layout["size"]
            if size:
                if size[0]:
                    x_sizes.append(size[0])
                if size[1]:
                    y_sizes.append(size[1])
            px, py = next(item["offset"] for item in self.conf_a["slots"]["list"] if item["name"] == slot_name)
            if (px is not None) and (py is not None):
                enabled = []  # which pixels are on?
                siter = self.iv_store.get_iter_from_string(f"0:{i}")  # substrate iterator
                diter = self.iv_store.iter_children(siter)  # device iterator
                while diter is not None:  # loop over devices on substrate
                    row = self.iv_store[diter]
                    picked = row[1]
                    enabled.append(picked)
                    diter = self.iv_store.iter_next(diter)
                lod = self.draw_layout(layout, angle, enabled)[0]
                g = drawsvg.Group(**{"transform": f"translate({px},{py})"})
                for e in lod.all_elements():
                    g.append(e)

                if size:
                    lab = drawsvg.Text(slot_name, big_font_size, -size[0] / 2, size[1] / 2, fill="lime", stroke=None, fill_opacity=None, stroke_width="0.4%", style="font-family: monospace", **{"font-weight": "bold", "transform": f"rotate({-angle},0,0)"})
                else:
                    bfs = big_font_size * 2
                    lab = drawsvg.Text(slot_name, bfs, 0, 0, fill="lime", stroke=None, fill_opacity=None, stroke_width="0.4%", style="font-family: monospace", **{"dy": ".4em", "text-anchor": "middle", "font-weight": "bold", "transform": f"rotate({-angle},0,0)"})

                g.append(lab)
                rg.append(g)

        cx = max(x_cens) - min(x_cens) + max(x_sizes)
        cy = max(y_cens) - min(y_cens) + max(y_sizes)

        d = drawsvg.Drawing(cx, cy, origin="center", displayInline=False, **{"text-align": "center", "font-family": "monospace"})
        d.append(rg)
        # d = draw.Drawing(max(canvas), max(canvas), origin='center', displayInline=False)
        # cmax = max(canvas)

        # try to see if we can hyperlink the background...
        # TODO: does not work atm, remove if this can't be fixed
        # class Hyperlink(draw.DrawingParentElement):
        #    TAG_NAME = 'a'
        #    def __init__(self, href, target=None, **kwargs):
        #        super().__init__(href=href, target=target, **kwargs)

        # hlink = Hyperlink('https://www.python.org', target='_blank')
        # hlink.append(draw.Rectangle(-canvas[0]/2, -canvas[1]/2, canvas[0], canvas[1], fill='white'))
        # d.append(hlink)
        # rh = self.array_drawing_handle
        # angle = self.conf_a['UI']['gui_drawing_rotation_angle']
        # cairo_context.rotate(math.pi*angle/180)
        # rh.render_cairo(cairo_context)
        # print(rh.get_geometry_for_element(None)[-1].width)

        # maxd = max(canvas)
        # scale = max_render_pix/maxd
        # d.setPixelScale(scale)
        svgd = d.as_svg()
        if isinstance(svgd, str):
            svg_handle = Rsvg.Handle.new_from_data(svgd.encode())
            vb = svg_handle.get_intrinsic_dimensions().out_viewbox
            r = svg_handle.get_geometry_for_layer(None, vb).out_ink_rect
            vb_new = (r.x, r.y, r.width, r.height)
            d.view_box = vb_new
            d.width = r.width
            d.height = r.height
            ndims = [r.width, r.height]
            scale = max_render_pix / max(ndims)
            d.set_pixel_scale(scale)
            svgd = d.as_svg()
            if isinstance(svgd, str):
                svg_handle = Rsvg.Handle.new_from_data(svgd.encode())

                self.array_drawing_handle = svg_handle

                self.array_pic.props.width_request = ndims[0] * scale
                self.array_pic.props.height_request = ndims[1] * scale

    def on_array_pic_draw(self, drawing_area, cairo_context):
        if self.array_drawing_handle is None:
            drawing_area.queue_draw()
        else:
            rh = self.array_drawing_handle
            # angle = self.conf_a['UI']['gui_drawing_rotation_angle']
            # cairo_context.rotate(math.pi*angle/180)
            rh.render_cairo(cairo_context)

    # draws pixels based on layout info from the config file
    def draw_layout(self, layout: dict, angle: float, enabled: list[bool] | None = None):
        max_render_pix = 300  # how big in pixels should the canvas be on screen?
        if (not layout["size"]) or (None in layout["size"]):
            canvas = [0, 0]
        else:
            canvas = layout["size"]
        maxd = max(canvas)
        if maxd:
            scale = round(max_render_pix / maxd)
        else:
            scale = max_render_pix

        d = drawsvg.Drawing(canvas[0], canvas[1], origin="center", displayInline=False, **{"text-align": "center", "font-family": "monospace"})
        dr = drawsvg.Drawing(canvas[0], canvas[1], origin="center", displayInline=False, **{"text-align": "center", "font-family": "monospace"})

        n_pads = len(layout["pads"])

        if canvas[0] and canvas[1]:
            wbg = drawsvg.Rectangle(-canvas[0] / 2, -canvas[1] / 2, canvas[0], canvas[1], fill="white")
            d.append(wbg)

        if (n_pads > 0) and (len(layout["size"]) == 2) and (None not in layout["size"]):  # handles the empty or non-sized substrate case
            bg = drawsvg.Rectangle(-layout["size"][0] / 2, -layout["size"][1] / 2, layout["size"][0], layout["size"][1], fill="black")
            d.append(bg)

        loc = layout["locations"]
        loc = [(x, y * -1) if y is not None else (x, y) for x, y in loc]
        for i, pad in enumerate(layout["pads"]):
            if enabled is None:
                fill = "white"
            else:
                try:
                    if enabled[i]:
                        fill = "yellow"
                    else:
                        fill = "red"
                except:
                    fill = "white"
            if layout["areas"][i] == -1:  # handle custom area case
                continue
            a = layout["areas"][i] * 100  # cm^2 to mm^2
            xy = loc[i]
            shape = layout["shapes"][i]
            if shape == "c":
                r = math.sqrt(a / math.pi)
                d.append(drawsvg.Circle(xy[0], xy[1], r, fill=fill))
            elif shape == "s":
                rx = math.sqrt(a)
                d.append(drawsvg.Rectangle(xy[0] - rx / 2, xy[1] - rx / 2, rx, rx, fill=fill))
            elif isinstance(shape, float):
                rx = shape
                ry = a / rx
                d.append(drawsvg.Rectangle(xy[0] - rx / 2, xy[1] - ry / 2, rx, ry, fill=fill))
            lab_font_size = scale / 5
            lx = xy[0]
            ly = xy[1]  # - lab_font_size / 3
            # urot = f"rotate({-angle},{lx},{ly})"
            lab = drawsvg.Text(str(pad), lab_font_size, lx, ly, fill="gray", **{"text-anchor": "middle", "font-weight": "bold", "dy": ".4em"})  # , "transform":urot
            d.append(lab)
        if ("OLD" in layout["name"]) or ("legacy" in layout["name"]):
            ll = maxd / 4
            x1 = drawsvg.Line(ll, ll, -ll, -ll, **{"stroke-width": f"{ll/8}", "stroke": "red"})
            d.append(x1)
            x2 = drawsvg.Line(ll, -ll, -ll, ll, **{"stroke-width": f"{ll/8}", "stroke": "red"})
            d.append(x2)

        d.set_pixel_scale(scale)

        # now for the rotated version of this
        rot = f"rotate({angle},0,0)"
        g = drawsvg.Group(**{"transform": rot})
        for e in d.all_elements():
            g.append(e)
        dr.append(g)
        dr.set_pixel_scale(scale)

        return d, dr

    def load_live_data_webviews(self, load, *arg):
        for i, wvid in enumerate(self.wvids):
            wv = self.b.get_object(wvid)
            if wv.get_visible() == True:
                wv.stop_loading()
                if load == True:
                    wv.load_uri(self.uris[i])
                else:
                    wv.load_html("Inactive.")

    # must only get called if this tab is visible
    def load_custom_webview(self, load, *arg):
        wv = self.b.get_object("custom_wv")
        if wv.get_visible() == True:
            wv.stop_loading()
            if load == True:
                wv.load_uri(self.uris[-1])  # should always be the last webview and uri
            else:
                wv.load_html("Inactive.")

    # gets called when the user selects a custom position
    def on_load_pos(self, cb):
        j = cb.get_active()
        pos = self.custom_coords[j]
        for i, coord in enumerate(pos):
            self.gotos[i].set_value(coord)

    # fires every time a row is changed in the slot config store
    def on_slot_store_change(self, store, path, iter, user_data):
        assert self.conf_a, "Config not properly loaded."
        col = user_data[0]
        do_dev_store_update_tasks = user_data[1]
        slot = store[iter][0]
        user_label = store[iter][1]
        layout_name = store[iter][2]

        this_layout = next(item for item in self.conf_a["substrates"]["layouts"] if item["name"] == layout_name)

        try:
            pads = this_layout["pads"]
            n_pix = len(pads)
            areas = this_layout["areas"]
        except:
            pads = []
            n_pix = float("nan")
            areas = []

        for store in [self.iv_store]:
            # store.set_sort_column_id(Gtk.TREE_SORTABLE_UNSORTED_SORT_COLUMN_ID, Gtk.SortType.ASCENDING)  # ensure the rows are sorted correctly for display
            all_row = store.get_iter_first()
            slot_iter = store.iter_nth_child(all_row, int(str(path)))

            if col == 1:  # user label change
                # update the label in the device selection stores
                if user_label != "":
                    display_label = f"{slot}: {user_label}"
                else:
                    display_label = slot
                store.set_value(slot_iter, 0, display_label)
            elif col == 2:  # this was a layout change
                subs_check_visible = n_pix > 0
                store.set_value(slot_iter, 3, layout_name)
                store.set_value(slot_iter, 4, subs_check_visible)

                # is the parent checked?
                if store[slot_iter][1] == True:
                    checked = True
                else:
                    checked = False

                # re-fill the children with the updated info
                diter = store.iter_children(slot_iter)
                for pad, area in zip(pads, areas):
                    name_col_val = f"Device {pad}"  # what do we call this?
                    checked_col_val = checked  # is it selected?
                    expanded_col_val = False  # is the row expanded?
                    area_col_val = f"{area:,.3f} cm<sup>2</sup>"  # what's its area?
                    check_vis_col_val = True  # is the selection box visible?

                    if diter is None:  # we need to add a new row
                        diter = store.append(slot_iter, [name_col_val, checked_col_val, expanded_col_val, area_col_val, check_vis_col_val])
                    else:  # the row already exists let's just make sure it's right
                        store.set(diter, 0, name_col_val, 1, checked_col_val, 2, expanded_col_val, 3, area_col_val, 4, check_vis_col_val)
                    diter = store.iter_next(diter)

                # delete any extra children
                while (diter is not None) and (store.iter_is_valid(diter)):
                    store.remove(diter)
            # store.set_sort_column_id(0, Gtk.SortType.ASCENDING)  # ensure the rows are sorted correctly for display

            if do_dev_store_update_tasks:
                GLib.idle_add(self.do_dev_store_update_tasks, store)

    # this function does the things that need to be done when something about a device store has changed
    # such as areas changed, pixels added to or removed from substrate, user label change or device selection change
    # it
    # - ensures the device store ckeckbox booleans make sense.
    # by potentially modifying [2] incosistent, [4] visible and [1] selected values for the top and substrate rows
    # - won't ever modify device row selections
    # - fills the bitmask text field
    # - writes the dev selection string
    # - computes the device dataframe containing relevant info on every device selected for measurement
    def do_dev_store_update_tasks(self, store):
        assert self.conf_a, "Config not properly loaded."
        # store.set_sort_column_id(0, Gtk.SortType.ASCENDING)  # ensure the rows are sorted correctly for display
        all_row = store.get_iter_first()  # the all row iterator
        siter = store.iter_children(all_row)  # the substrate iterator
        n_total = 0
        n_total_selected = 0
        bitmask = 0
        bitloc = 0

        df_cols = []
        df_cols.append("slot")  # label for the substrate slot that the system uses
        df_cols.append("user_label")  # label the user may have entered for this substrate
        df_cols.append("label")  # combo of the above two, formated as "{system}_{user}"
        df_cols.append("device_label")  # formated as "{system}:{user}, Device {mux_index}" or "{system}, Device {mux_index}"
        df_cols.append("substrate_index")  # a number used to represent the substrate
        df_cols.append("layout_pixel_index")  # pixel index is for which pixel this is on its layout
        df_cols.append("pixel_offset_raw")  # offset of this pixel relative to the center of its substrate (as read from the config file)
        # df_cols.append("pixel_offset")  # offset of this pixel relative to the center of its substrate (with unconfigured axes trimmed)
        df_cols.append("substrate_offset_raw")  # offset of this substrate in the array
        # df_cols.append("substrate_offset")  # offset of this substrate in the array (with unconfigured axes trimmed)
        # df_cols.append("loc_raw")  # offset of this pixel from center of substrate array
        # df_cols.append("loc")  # offset of this pixel from center of substrate array  (with unconfigured axes trimmed)
        df_cols.append("layout")  # the layout name
        df_cols.append("area")  # illuminated area in cm^2
        df_cols.append("dark_area")  # active area in cm^2
        df_cols.append("pad")  # mux index corresponds to a pixel from the mux's point of view (same as "pad" in cofig file)
        # df_cols.append("mux_string")  # the string the firmware needs to select this pixel
        # df_cols.append("sort_string")  # the string we'll use for a custom user supplied testing order
        df_cols.append("user_vars")  # dictionary for the user variables
        # append variable cols carefully to not repeat built in ones
        uvs = self.variables.copy()
        for i, var in enumerate(uvs):
            if var in df_cols:
                var = f"V_{var}"  # if there's a name conflict, prepend it with V_
                uvs[i] = var
            df_cols.append(var)

        big_dict = {}  # TODO: refactor this into a list of dicts instead of a dict of lists
        for col in df_cols:
            big_dict[col] = []  # start with a dict where all values are empty lists
        self.slot_bitmasks = []
        while siter is not None:  # substrate iterator loop
            slot_bitmask = ""
            n_subs = store.iter_n_children(siter)
            n_total += n_subs

            # set substrate level box visibility
            if n_subs == 0:
                # this substrate's picker is invisible
                store.set_value(siter, 4, False)
            else:
                store.set_value(siter, 4, True)

            n_subs_selected = 0
            spath = str(store.get_path(siter))
            spath_split = spath.split(":")
            subi = int(spath_split[-1])  # substrate index in tree
            slot = self.slot_config_store[subi][0]  # slot name string
            user_label = self.slot_config_store[subi][1]
            layout_name = self.slot_config_store[subi][2]
            layout = next(item for item in self.conf_a["substrates"]["layouts"] if item["name"] == layout_name)

            diter = store.iter_children(siter)  # the device iterator
            while diter is not None:  # device iterator loop
                selected = store[diter][1]
                if selected:
                    slot_bitmask += "1"
                    bitmask += 1 << bitloc

                    tbl_dev_lbl = store[diter][0]  # like "Device 3"
                    pad = int(tbl_dev_lbl.removeprefix("Device"))  # pad number
                    padi = layout["pads"].index(pad)  # pad index (position in pad list)
                    big_dict["layout_pixel_index"].append(padi)
                    big_dict["substrate_index"].append(subi)
                    big_dict["slot"].append(slot)
                    big_dict["pad"].append(pad)
                    big_dict["user_label"].append(user_label)
                    if user_label == "":
                        big_dict["label"].append(slot)
                        big_dict["device_label"].append(f"{slot}:Dev {pad}")
                    else:
                        big_dict["label"].append(f"{slot}_{user_label}")
                        big_dict["device_label"].append(f"{slot}({user_label}):Dev {pad}")
                    big_dict["layout"].append(layout_name)
                    big_dict["area"].append(layout["areas"][padi])
                    big_dict["dark_area"].append(layout["dark_areas"][padi])
                    por = layout["locations"][padi]
                    big_dict["pixel_offset_raw"].append(por)
                    sor = next(item["offset"] for item in self.conf_a["slots"]["list"] if item["name"] == slot)
                    big_dict["substrate_offset_raw"].append(sor)
                    user_vars = {}
                    for i, var in enumerate(uvs):
                        big_dict[var].append(self.slot_config_store[subi][3 + i])
                        user_vars[var] = self.slot_config_store[subi][3 + i]
                    big_dict["user_vars"].append(user_vars)

                    n_subs_selected += 1
                else:
                    slot_bitmask += "0"

                bitloc += 1
                diter = store.iter_next(diter)
            n_total_selected += n_subs_selected
            self.slot_bitmasks.append(slot_bitmask[::-1])

            # set substrate level box state
            if n_subs_selected == 0:
                store.set_value(siter, 1, False)
                store.set_value(siter, 2, False)  # set consistent
            elif n_subs_selected == n_subs:
                store.set_value(siter, 1, True)
                store.set_value(siter, 2, False)  # set consistent
            else:
                store.set_value(siter, 1, False)
                store.set_value(siter, 2, True)  # set inconsistent

            siter = store.iter_next(siter)

        # set gui's hex text
        fmt_str = f"0{math.ceil(bitloc/4)}X"
        bitmask_text = f"0x{bitmask:{fmt_str}}"
        store_name = store.get_name()
        if "IV" in store_name:
            self.iv_dev_box.set_text(bitmask_text)
        # elif "EQE" in store_name:
        #    self.eqe_dev_box.set_text(bitmask_text)

        # attach the active pixel big_dict we just made to the store
        store.bd = {store_name.split()[0]: big_dict}

        # set top level box visiblility
        if n_total == 0:
            # top level picker is invisible
            store.set_value(all_row, 4, False)
        else:
            store.set_value(all_row, 4, True)

        # set top level checkbox state
        if n_total_selected == 0:
            store.set_value(all_row, 1, False)
            store.set_value(all_row, 2, False)  # set consistent
        elif n_total_selected == n_total:
            store.set_value(all_row, 1, True)
            store.set_value(all_row, 2, False)  # set consistent
        else:
            store.set_value(all_row, 1, False)
            store.set_value(all_row, 2, True)  # set inconsistent

    def do_command_line(self, command_line):
        self.lg.debug("Doing command line things")
        options = command_line.get_options_dict()
        upd_options = options.end().unpack()

        if "max-runtime" in upd_options:
            if upd_options["max-runtime"] > 0:
                # if configured, quit the app after some number of seconds (used for testing)
                GLib.timeout_add_seconds(upd_options["max-runtime"], self.terminate, None)
                self.lg.debug(f'This program will terminate in {upd_options["max-runtime"]}s')

        if "conf-file" in upd_options:
            auxconfs = upd_options["aux-confs"] + upd_options[GLib.OPTION_REMAINING]
            self.cli_conf_paths = []
            for aux_conf in auxconfs:
                self.cli_conf_paths.append(pathlib.Path(bytes(aux_conf).decode()))
            self.lg.debug(f"CLI provided aux confs: {self.cli_conf_paths}")

        if "mem-db-url" in upd_options:
            self.cli_mem_db_url = upd_options["mem-db-url"]
            self.lg.debug(f"CLI provided memory db url: {self.cli_mem_db_url}")

        self.activate()
        return 0

    # adds text, then scrolls the log window to the bottom
    # (called from GLib.idle_add or else segfault!)
    def append_to_log_window(self, text):
        if self.terminating == False:
            ei = self.logTB.get_end_iter()
            self.logTB.insert(ei, text)
            adj = self.log_win_adj
            adj.set_value(adj.get_upper())

    def on_about(self, action, param):
        about_dialog = Gtk.AboutDialog(transient_for=self.main_win, modal=True)
        about_dialog.run()

    def do_shutdown(self):
        self.terminating = True

        # stop the ticker
        if self.ticker_id:
            GLib.source_remove(self.ticker_id)

        # disconnect MQTT
        if self.mqttc:
            self.mqttc.disconnect()
        if self.mqtt_thread:
            self.mqtt_thread.join()

        # remove gui log handler
        for h in self.lg.handlers:
            if h.get_name() == "ui":
                self.lg.removeHandler(h)
                self.lg.debug("Shutting down")

        Gtk.Application.do_shutdown(self)

    def on_debug_button(self, *args, **kw_args):
        self.do_debug_tasks()

    def on_destroy_btn_clicked(self, *args, **kw_args):
        self.lg.warning("This incident will be reported.")

    def do_debug_tasks(self, *args, **kw_args):
        self.lg.debug("Hello World!")
        # self.b.get_object("run_but").set_sensitive(True)
        # self.load_live_data_webviews(load=False)
        # msg = {"cmd": "debug"}
        # j_msg = json.dumps(msg)
        # self.emit_message("cmd/util", j_msg, qos=2)
        # print(f"{self.variables=}")
        # print(f"{type(self.conf_a['smu'])=}")
        # print(f"{self.conf_a['smu']=}")
        # self.draw_array()

    # fills the device selection bitmask text box based on the device selection treestore
    def open_dev_picker(self, button):
        button_name = button.get_name()
        if "IV" in button_name:
            self.show_dev_picker(self.iv_store, button)

    def show_dev_picker(self, store, button):
        self.device_select_tv.set_model(store)
        self.device_select_tv.expand_row(Gtk.TreePath("0"), False)  # top is always expanded
        self.po.set_relative_to(button)
        self.po.show_all()

    def on_pause_button(self, button):
        """Pause experiment operation."""
        self.lg.log(29, "Pausing run")
        # TODO: consider implimenting this
        # self.emit_message("gui/pause", "pause", qos=2)

    def on_stop_button(self, button):
        """Stop experiment operation."""
        self.lg.log(29, "Stopping run. Please wait up to a few seconds for confirmation...")
        self.b.get_object("progress").set_visible(False)
        self.emit_message("measurement/stop", json.dumps("stop"), qos=2)

    def on_spectrum_button(self, button):
        """The user clicked the spectrum button"""
        assert self.conf_a, "Config not properly loaded."
        self.lg.log(29, "Requesting spectrum data from solar sim...")
        msg = {}
        msg["cmd"] = "spec"
        msg["solarsim"] = self.conf_a["solarsim"]
        msg["recipe"] = self.b.get_object("light_recipe").get_text()
        msg["intensity"] = int(self.b.get_object("light_recipe_int").get_text())
        j_msg = json.dumps(msg)
        self.want_spectrum = True
        self.emit_message("cmd/util", j_msg, qos=2)

    def on_load_last_button(self, button):
        last = self.find_last()
        if last is not None:
            try:
                self.load_file(last)
            except Exception as e:
                self.lg.warning(str(e))
        else:
            self.lg.warning("Couldn't find last run config")

    def find_last(self):
        """returns the last gui state auto save file (if possible)"""
        assert self.conf_a, "Config not properly loaded."
        ret = None

        try:
            user_autosave_path = self.conf_a["meta"]["autosave_path"]
        except:
            user_autosave_path = "runconfigs"
        if pathlib.Path(user_autosave_path).is_absolute():
            autosave_root = pathlib.Path(user_autosave_path)
        else:
            autosave_root = pathlib.Path.home() / user_autosave_path

        if autosave_root.is_dir():
            glob_matches = list(autosave_root.rglob("*_autosave.gsf.json"))
            if len(glob_matches) > 0:
                ret = max(glob_matches, key=os.path.getmtime)
                if not ret.is_file:
                    ret = None
            else:
                ret = None

        # update button sensitivity
        if ret is None:
            self.b.get_object("load_last").set_sensitive(False)
        else:
            self.b.get_object("load_last").set_sensitive(True)

        return ret

    def harvest_gui_data(self):
        """Packages up all the (relevant) info the user has entered into the gui
        Used when the user saves the gui state or clicks the button to start a measurement run"""
        gui_data = {}
        for id_str in self.ids:
            if not id_str.startswith("___"):  # ignore ids that don't have their value explicitly set
                this_obj = self.b.get_object(id_str)
                if any([isinstance(this_obj, x) for x in [gi.repository.Gtk.Switch, gi.repository.Gtk.CheckButton, gi.repository.Gtk.ToggleButton, gi.overrides.Gtk.ComboBox, gi.repository.Gtk.CheckButton]]):  # type: ignore
                    gui_data[id_str] = {"type": str(type(this_obj)), "value": this_obj.get_active(), "call_to_set": "set_active"}
                elif isinstance(this_obj, gi.repository.Gtk.SpinButton):  # type: ignore
                    gui_data[id_str] = {"type": str(type(this_obj)), "value": this_obj.get_value(), "call_to_set": "set_value"}
                elif isinstance(this_obj, gi.repository.Gtk.Entry):  # type: ignore
                    gui_data[id_str] = {"type": str(type(this_obj)), "value": this_obj.get_text(), "call_to_set": "set_buffer_text"}

        # handle the treestores and liststores manually because they can't be pickled, plus they're not registered in glade
        for store in ["iv_store", "slot_config_store"]:
            this_obj = getattr(self, store)
            store_data = []
            this_obj.foreach(lambda model, path, it: store_data.append([str(path), tuple(model[it])]))
            gui_data[store] = {"type": str(type(this_obj)), "value": store_data, "call_to_set": "fill_store"}

        return gui_data

    def on_save_button(self, button):
        """Save current state of widget entries to a file."""
        save_dialog = Gtk.FileChooserNative(
            title="Pick a place to save to",
            transient_for=self.main_win,
            action=Gtk.FileChooserAction.SAVE,
        )
        filt = Gtk.FileFilter()
        filt.add_pattern("*.gsf.json")
        # filt.add_suffix("*.gsf.json")
        filt.add_mime_type("application/json")
        filt.set_name("GUI State Files (*.gsf.json)")
        save_dialog.add_filter(filt)

        filt = Gtk.FileFilter()
        filt.add_pattern("*")
        filt.set_name("All Files")
        save_dialog.add_filter(filt)

        save_dialog.set_current_name("gui_state.gsf.json")

        response = save_dialog.run()
        if response == Gtk.ResponseType.ACCEPT:
            this_file = save_dialog.get_filename()
            slots_file = f"{this_file.removesuffix('.json').removesuffix('.gsf')}_slots.tsv"
            self.lg.log(29, f"Saving gui state to: {this_file}")

            savedata = self.make_data_package()
            slot_dict_list = savedata.pop("conf_c")

            with open(slots_file, "w", newline="") as csvfile:
                writer = csv.DictWriter(csvfile, fieldnames=slot_dict_list[0].keys(), dialect="excel-tab")
                writer.writeheader()
                [writer.writerow(row) for row in slot_dict_list]

            with open(this_file, "w") as f:
                json.dump(savedata, f)
        else:
            self.lg.log(29, f"Save aborted.")

    def make_data_package(self):
        """smashes run, gui and config data together"""
        self.do_dev_store_update_tasks(self.iv_store)

        # raw data from the gui's objects, needed for full state restore (saving/loading)
        guidata = self.harvest_gui_data()
        jguidatab = json.dumps(guidata).encode()
        digest = hmac.digest(self.hk, jguidatab, "sha1")
        guidata["digest"] = f"0x{digest.hex()}"

        # prune/reformat raw gui data to make it more suitable for external consumption
        conf_b = self.mk_conf_b(guidata)

        rundata = {}
        rundata["conf_a"] = self.conf_a
        rundata["conf_b"] = conf_b
        jrundatab = json.dumps(rundata).encode()
        digest = hmac.digest(self.hk, jrundatab, "sha1")
        rundata["digest"] = f"0x{digest.hex()}"

        # now let's make conf_c (the unprotected slot config data)
        conf_c_keys = self.slot_table_base_headings + ["Bitmask"] + self.variables

        conf_c = []  # the slot config data table list of dicts
        for row in guidata["slot_config_store"]["value"]:
            conf_c_line = {}  # order of insertion is important with this one!
            row_num = int(row[0])
            row_vals = row[1]
            for key in conf_c_keys:
                if key in self.slot_table_base_headings:
                    row_i = self.slot_table_base_headings.index(key)
                    conf_c_line[key] = row_vals[row_i]
                elif key == "Bitmask":
                    bitmask = self.slot_bitmasks[row_num]
                    if bitmask == "":
                        bitmask = "0"
                    hex_bitmask = f"0x{int(bitmask, 2):X}"
                    conf_c_line[key] = hex_bitmask
                elif key in self.variables:
                    row_i = len(self.slot_table_base_headings) + self.variables.index(key)
                    conf_c_line[key] = row_vals[row_i]
                else:
                    raise KeyError("Unexpected config type C key")
            conf_c.append(conf_c_line)

        return {"guidata": guidata, "rundata": rundata, "conf_c": conf_c}

    def load_file(self, file: pathlib.Path):
        """restore the gui's state from a config file"""
        self.lg.log(29, f"Loading gui state from: {file}")

        strfile = str(file)
        if strfile.endswith(".gsf.json"):
            try:
                with open(strfile, "r") as f:
                    raw_load = json.load(f)
            except Exception as e:
                raise ValueError(f"Failure loading {strfile}: {e}")
        else:
            raise ValueError(f"File has unknown extension: {strfile}")

        # ids that we don't want to load into the gui
        dont_load = ["run_name_suffix", "run_name", "db_con_str", "db_conf_num"]

        if "guidata" in raw_load:
            load_data = raw_load["guidata"]
            remotedigest = bytes.fromhex(load_data.pop("digest").removeprefix("0x"))
            jload_datab = json.dumps(load_data).encode()
            localdigest = hmac.digest(self.hk, jload_datab, "sha1")
            if remotedigest != localdigest:
                raise ValueError(f"Can't load {strfile}")
        else:
            load_data = raw_load  # TODO: remove this after transition period because it has no hmac check

        # pop away the things we don't want
        [load_data.pop(key, None) for key in dont_load]

        stores = {}  # a place to hold the stores we read. they need to be filled last
        for id_str, obj_info in load_data.items():
            this_type = obj_info["type"]
            if ("TreeStore" in this_type) or ("ListStore" in this_type):  # for handling liststores and treestores
                stores[id_str] = obj_info  # save the stores for later handling when we see them
            elif this_type == "variables":  # deal with the user's custom variables here
                vars = obj_info["value"]
                default_layouts, default_checkmarks, default_areas, default_pads = self.gen_defaults()
                self.reset_slot_config(default_layouts)
                for var in vars:
                    self.add_variable(var)
            else:  # all the other gui params can be handled simply
                try:
                    this_obj = self.b.get_object(id_str)
                    if obj_info["call_to_set"] == "set_buffer_text":
                        b = this_obj.get_buffer()
                        b.set_text(obj_info["value"], -1)
                    else:
                        call_to_set = getattr(this_obj, obj_info["call_to_set"])
                        call_to_set(obj_info["value"])
                except:
                    self.lg.log(29, f"Load issue with: {id_str}")
                    # pass  # give up if we can't load this one element

        # now we can update the stores since we know we have shape for the slot config store set properly
        # but the slot config one has to go first
        if "slot_config_store" in stores.keys():
            self.load_store("slot_config_store", stores["slot_config_store"])
            del stores["slot_config_store"]

        # now we can load the rest of the stores
        for id_str, obj_info in stores.items():
            self.load_store(id_str, obj_info)
        self.update_gui()

    def on_slot_load_but(self, button):
        """load an an external file for the slot config table"""
        slot_load_dialog = Gtk.FileChooserNative(
            title="Choose a file for the slot config table",
            transient_for=self.main_win,
            action=Gtk.FileChooserAction.OPEN,
        )

        filt = Gtk.FileFilter()
        filt.add_pattern("*.tsv")
        filt.add_pattern("*.txt")
        filt.set_name("Tab separated values (*.tsv, *.txt)")
        slot_load_dialog.add_filter(filt)

        filt = Gtk.FileFilter()
        filt.add_pattern("*")
        filt.set_name("All Files")
        slot_load_dialog.add_filter(filt)

        response = slot_load_dialog.run()
        if response == Gtk.ResponseType.ACCEPT:
            this_file = slot_load_dialog.get_filename()
            try:
                self.populate_slot_table(pathlib.Path(this_file))
            except Exception as e:
                self.lg.warning(str(e))
        else:
            self.lg.log(29, f"Load aborted.")

    def populate_slot_table(self, filepath: pathlib.Path):
        """fills the slot config table with vals from an external file"""
        assert self.conf_a, "Config not properly loaded."
        slot_names = [s["name"] for s in self.conf_a["slots"]["list"]]
        filestr = str(filepath)
        try:
            with open(filestr, newline="") as csvfile:
                reader = csv.DictReader(csvfile, dialect="excel-tab")
                if reader.fieldnames:
                    headers = list(reader.fieldnames)
                else:
                    headers = []
                dictlist = list(reader)
        except Exception as e:
            raise ValueError(f"Could not load {filestr}: {e}")
        else:  # no exception loading file
            try:
                # check header format
                variables = headers.copy()  # will be pruned down below
                required_cols = self.slot_table_base_headings + ["Bitmask"]
                # r_index = []  # where these required cols are
                for rcol in required_cols:
                    assert rcol in headers
                    variables.remove(rcol)
                    # r_index.append(col_names.index(rcol))
            except Exception as e:
                raise ValueError(f"Failed to parse {filestr}: {e}")
            else:  # headers check out
                try:  # read the data
                    # make the store
                    slot_config_store = {}
                    slot_config_store["type"] = "ListStore"
                    slot_config_store["value"] = []  # slot store data goes here
                    # iv_store = {"type": "TreeStore"}
                    # iv_store["value"] = []  # iv store (pixel selection) data goes here
                    checkmarks = []  # list of lists of bools that tells us which pixels are selected
                    user_labels = []  # list of user lables for going into the device picker store
                    layouts = []  # list of layouts for going into the device picker store
                    areas = []  # list of layouts for going into the device picker store
                    pads = []  # list of layouts for going into the device picker store

                    for i, data in enumerate(dictlist):
                        slot = data["Slot"]
                        user_label = data["Label"]
                        layout_name = data["Layout"]
                        layout = next(item for item in self.conf_a["substrates"]["layouts"] if item["name"] == layout_name)
                        bm_val = int(data["Bitmask"].removeprefix("0x"), 16)
                        bitmask = bin(bm_val).removeprefix("0b")
                        assert slot == slot_names[i]  # ensure system slot label match perfection
                        subs_areas = layout["areas"]  # list of pixel areas for this layout
                        subs_pads = layout["pads"]  # list of pixel areas for this layout
                        assert 2 ** len(subs_pads) >= bm_val  # make sure the bitmask hex isn't too big for this layout
                        subs_cms = [x == "1" for x in f"{bitmask:{'0'}{'>'}{len(subs_areas)}}"][::-1]  # checkmarks (enabled pixels) for this substrate
                        checkmarks.append(subs_cms)

                        datalist = []
                        datalist.append(slot)
                        datalist.append(user_label)
                        user_labels.append(user_label)
                        datalist.append(layout_name)
                        layouts.append(layout_name)
                        areas.append(subs_areas)
                        pads.append(subs_pads)

                        for variable in variables:
                            datalist.append(data[variable])
                        slot_config_store["value"].append([str(i), datalist])

                    default_layouts, default_checkmarks, default_areas, default_pads = self.gen_defaults()
                    self.reset_slot_config(default_layouts)
                    for variable in variables:
                        self.add_variable(variable)
                    self.load_store("slot_config_store", slot_config_store)

                    self.fill_device_select_store(self.iv_store, checkmarks, [s["name"] for s in self.conf_a["slots"]["list"]], user_labels, layouts, areas, pads)
                    GLib.idle_add(self.do_dev_store_update_tasks, self.iv_store)
                    # self.handle_custom_area_box()
                except AssertionError:
                    raise ValueError(f"A check failed while loading {filestr}")
                except Exception as e:
                    raise ValueError(f"Failure building slot or device selection table {filestr}: {e}")

    def on_open_button(self, button):
        """Populate widget entries from data saved in a file."""
        open_dialog = Gtk.FileChooserNative(
            title="Pick a file to load from",
            transient_for=self.main_win,
            action=Gtk.FileChooserAction.OPEN,
        )

        filt = Gtk.FileFilter()
        filt.add_pattern("*.gsf.json")
        filt.set_name("GUI State Files (*.gsf.json)")
        open_dialog.add_filter(filt)

        filt = Gtk.FileFilter()
        filt.add_pattern("*")
        filt.set_name("All Files")
        open_dialog.add_filter(filt)

        response = open_dialog.run()
        if response == Gtk.ResponseType.ACCEPT:
            this_file = open_dialog.get_filename()
            try:
                self.load_file(pathlib.Path(this_file))
            except Exception as e:
                self.lg.warning(str(e))
        else:
            self.lg.log(29, f"Load aborted.")

    def load_store(self, what, info):
        """load a store from the save file format into the gui's GTK format"""
        try:
            store = getattr(self, what)
            for line in info["value"]:
                try:
                    path_str = line[0]
                    row_dat = line[1]
                    path = Gtk.TreePath.new_from_string(path_str)
                    itr = store.get_iter(path)
                    for i, itm in enumerate(row_dat):
                        try:
                            if what == "slot_config_store":
                                self.on_slot_cell_edit(None, path, itm, (i, True, False))
                            elif what == "iv_store":
                                store.set_value(itr, i, itm)
                        except Exception as e:
                            self.lg.debug(f"Error on store load row item entry: {repr(e)}")
                except Exception as e:
                    self.lg.debug(f"Error on store load row entry: {repr(e)}")
            if what == "iv_store":  # for the device stores
                # make sure the data frames are correct
                self.do_dev_store_update_tasks(store)
        except Exception as e:
            self.lg.debug(f"Failed to load store {what}: {repr(e)}")

    # looks at where the popover is
    # to determint which device selection store we want
    def get_store(self, target_id=""):
        # where is the popover right now?
        if target_id == "":
            target_id = Gtk.Buildable.get_name(self.po.get_relative_to())
        if "iv" in target_id:
            store = self.iv_store
        else:
            store = None
        return store

    def on_round_robin_button(self, button):
        assert self.conf_a, "Config not properly loaded."
        button_label = button.get_label()
        this_type = "none"
        if "Connect" in button_label:
            self.lg.log(29, "Connectivity check started...")
            this_type = "connectivity"
        elif "Volt" in button_label:
            self.lg.log(29, "Voltage check started...")
            this_type = "voltage"
        elif "Curr" in button_label:
            self.lg.log(29, "Current check started...")
            this_type = "current"
        elif "RTD" in button_label:
            self.lg.log(29, "Measuring RTD temperatures...")
            this_type = "rtd"

        # stack the eqe and iv bigdicts
        rbd = {}
        for bd in [self.iv_store.bd]:  # in [self.eqe_store.bd, self.iv_store.bd]
            for key, val in bd.items():
                for key2, val2 in val.items():
                    if key2 in rbd:
                        rbd[key2] += val2
                    else:
                        rbd[key2] = val2

        # all the row-slot pairs
        rs_pairs = list(zip(rbd["slot"], rbd["pad"]))

        # indicies of unique row-slot combos
        uidx = [rs_pairs.index(x) for x in set(rs_pairs)]

        msg = {}
        msg["cmd"] = "round_robin"
        msg["type"] = this_type
        msg["slots"] = [rbd["slot"][i] for i in uidx]
        msg["pads"] = [rbd["pad"][i] for i in uidx]
        msg["labels"] = [rbd["user_label"][i] for i in uidx]
        msg["dev_layouts"] = [rbd["layout"][i] for i in uidx]
        msg["layouts"] = self.conf_a["substrates"]["layouts"]
        msg["mc"] = self.conf_a["mc"]["address"]
        msg["mux"] = self.conf_a["mux"]["address"]
        msg["mc_virt"] = self.conf_a["mc"]["virtual"]
        msg["smus"] = self.conf_a["smus"]
        msg["db_uri"] = self.mem_db_url
        msg["conf_id"] = self.conf_a_id  # config type a id number
        msg["group_order"] = self.conf_a["slots"]["group_order"]

        j_msg = json.dumps(msg)
        self.emit_message("cmd/util", j_msg, qos=2)

    # this is for mux device toggle button in the utility view
    def on_device_toggle(self, button):
        """
        allows the user to connect any one pixel, or disconnect them all
        toggels between connecting the first selected IV device and selecting nothing
        """
        assert self.conf_a, "Config not properly loaded."
        if self.all_mux_switches_open == True:
            self.all_mux_switches_open = False
            name = list(self.iv_store.bd)[0]
            if len(self.iv_store.bd[name]["pad"]) == 0:
                self.lg.log(29, "No devices selected for connection")
            else:
                ul = self.iv_store.bd[name]["user_label"][0]
                if ul == "":
                    lblstr = self.iv_store.bd[name]["slot"][0]
                else:
                    lblstr = f'{self.iv_store.bd[name]["slot"][0]}({ul})'
                self.lg.log(29, f'Connecting device: {lblstr}:Dev {self.iv_store.bd[name]["pad"][0]}')
                msg = {}
                msg["cmd"] = "for_mux"
                msg["mc"] = self.conf_a["mc"]["address"]
                msg["mc_virt"] = self.conf_a["mc"]["virtual"]
                msg["mux"] = self.conf_a["mux"]["address"]
                msg["mux_cmd"] = (self.iv_store.bd[name]["slot"][0], self.iv_store.bd[name]["pad"][0])
                j_msg = json.dumps(msg)
                self.emit_message("cmd/util", j_msg, qos=2)
        else:
            self.all_mux_switches_open = True
            self.lg.log(29, "Disconnecting all devices")
            msg = {}
            msg["cmd"] = "for_mux"
            msg["mc"] = self.conf_a["mc"]["address"]
            msg["mc_virt"] = self.conf_a["mc"]["virtual"]
            msg["mux"] = self.conf_a["mux"]["address"]
            msg["mux_cmd"] = (None, None)
            j_msg = json.dumps(msg)
            self.emit_message("cmd/util", j_msg, qos=2)

    def on_health_button(self, button):
        assert self.conf_a, "Config not properly loaded."
        self.lg.log(29, "HEALTH CHECK INITIATED")
        msg = {}
        msg["cmd"] = "check_health"
        if self.enable_controller:
            msg["mc"] = self.conf_a["mc"]["address"]
            msg["mc_virt"] = self.conf_a["mc"]["virtual"]
        if self.enable_psu == True:
            msg["psu"] = self.conf_a["psu"]["address"]
            msg["psu_virt"] = self.conf_a["psu"]["virtual"]
        if self.enable_lia == True:
            msg["lia_address"] = self.conf_a["lia"]["address"]
            msg["lia_virt"] = self.conf_a["lia"]["virtual"]
        if self.enable_mono == True:
            msg["mono_address"] = self.conf_a["monochromator"]["address"]
            msg["mono_virt"] = self.conf_a["monochromator"]["virtual"]
        msg["smus"] = self.conf_a["smus"]
        msg["solarsim"] = self.conf_a["solarsim"]
        msg["recipe"] = self.b.get_object("light_recipe").get_text()
        msg["intensity"] = int(self.b.get_object("light_recipe_int").get_text())
        j_msg = json.dumps(msg)
        self.emit_message("cmd/util", j_msg, qos=2)

    def connection_warning_dialog(self, header, body, extbuttons):
        message_dialog = Gtk.MessageDialog(modal=True, destroy_with_parent=True, transient_for=self.main_win, message_type=Gtk.MessageType.WARNING, buttons=Gtk.ButtonsType.NONE, text=header)
        message_dialog.format_secondary_text(body)
        message_dialog.props.message_area.get_children()[1].modify_font(Pango.FontDescription("Dejavu Sans Mono"))
        for btn in extbuttons:
            if "Continue" in btn:
                message_dialog.add_button(btn, Gtk.ResponseType.ACCEPT)
            elif "Abort" in btn:
                message_dialog.add_button(btn, Gtk.ResponseType.CANCEL)
        message_dialog.set_default_response(Gtk.ResponseType.CANCEL)
        result = message_dialog.run()

        if result == Gtk.ResponseType.CANCEL:
            self.on_stop_button(None)
        elif result == Gtk.ResponseType.ACCEPT:
            self.emit_message("cmd/util", json.dumps("unblock"), qos=2)

        message_dialog.destroy()
        return False  # add idle will not reschedule

    def move_warning(self, msg=None, also_note=""):
        if (self.enable_stage == True) or (self.enable_zee == True):
            message_dialog = Gtk.MessageDialog(modal=True, destroy_with_parent=True, transient_for=self.main_win, message_type=Gtk.MessageType.WARNING, buttons=Gtk.ButtonsType.OK_CANCEL, text="This action may cause the stage to move.")
            if msg is None:
                to_show = "Before clicking OK, check that all foreign objects are clear from the stage area and that it is safe to move."
            else:
                to_show = msg

            if also_note != "":
                to_show = f"{to_show}\n{also_note}"

            message_dialog.format_secondary_text(to_show)

            result = message_dialog.run()
            message_dialog.destroy()
        else:
            result = Gtk.ResponseType.OK
        return result

    def on_home_button(self, button):
        self.do_home_stage(force=True)  # TODO: don't always force

    def force_home_stage(self, *args, **kw_args):
        self.do_home_stage(force=True)

    def do_home_stage(self, force=False):
        """Home the stage."""
        assert self.conf_a, "Config not properly loaded."
        if self.move_warning() == Gtk.ResponseType.OK:
            self.lg.log(29, "Requesting stage calibration...")
            msg = {}
            msg["cmd"] = "home"
            msg["force"] = force
            msg["mc"] = self.conf_a["mc"]["address"]
            msg["mc_virt"] = self.conf_a["mc"]["virtual"]
            msg["stage_uri"] = self.conf_a["motion"]["uri"]
            msg["stage_virt"] = self.conf_a["motion"]["virtual"]
            j_msg = json.dumps(msg)
            self.emit_message("cmd/util", j_msg, qos=2)

    def on_halt_button(self, button):
        """Emergency stop"""
        assert self.conf_a, "Config not properly loaded."
        self.lg.warning("Powering down the stage motor drivers")
        msg = {}
        msg["cmd"] = "estop"
        msg["mc"] = self.conf_a["mc"]["address"]
        msg["mc_virt"] = self.conf_a["mc"]["virtual"]
        j_msg = json.dumps(msg)
        self.emit_message("cmd/util", j_msg, qos=2)
        # self.emit_message("measurement/stop", "stop", qos=2)

    def on_stage_read_button(self, button):
        """Read the current stage position."""
        assert self.conf_a, "Config not properly loaded."
        msg = {}
        msg["cmd"] = "read_stage"
        msg["mc"] = self.conf_a["mc"]["address"]
        msg["mc_virt"] = self.conf_a["mc"]["virtual"]
        msg["stage_uri"] = self.conf_a["motion"]["uri"]
        msg["stage_virt"] = self.conf_a["motion"]["virtual"]
        j_msg = json.dumps(msg)
        self.emit_message("cmd/util", j_msg, qos=2)

    def on_goto_button(self, button):
        """Goto stage position."""
        assert self.conf_a, "Config not properly loaded."
        if self.move_warning() == Gtk.ResponseType.OK:
            self.lg.debug("Sending the stage some place")
            pos = [self.gotos[0].get_value()]
            if self.num_axes >= 2:
                pos += [self.gotos[1].get_value()]
            if self.num_axes >= 3:
                pos += [self.gotos[2].get_value()]
            msg = {}
            msg["cmd"] = "goto"
            msg["mc"] = self.conf_a["mc"]["address"]
            msg["mc_virt"] = self.conf_a["mc"]["virtual"]
            msg["stage_uri"] = self.conf_a["motion"]["uri"]
            msg["stage_virt"] = self.conf_a["motion"]["virtual"]
            msg["pos"] = pos
            j_msg = json.dumps(msg)
            self.emit_message("cmd/util", j_msg, qos=2)

    def is_usb_smu_config_good(self, final_check=False):
        assert self.conf_a, "Config not properly loaded."
        good = False

        # do sweep config verification
        if (self.smu_type == "ark-metrica") and (self.b.get_object("sweep_check").get_active()):
            if pversion.parse(self.conf_a["smus"][0]["version"]) <= pversion.parse("1.0.0"):
                steps = self.b.get_object("iv_steps").get_value()
                step_delay = self.b.get_object("source_delay").get_value() / 1000  # into seconds
                smu_nplc = self.b.get_object("nplc").get_value()
                plf = self.conf_a["smus"][0]["plf"]
                if steps * ((smu_nplc / plf) + step_delay) > 1:
                    if final_check == True:
                        self.lg.error("Invalid sweep settings detected")
                        self.lg.error("Sample time plus source delay can not be greater than 1/(number of steps)")
                        self.lg.error(f"But {(smu_nplc/plf)+step_delay:.4f} > {1/steps:.4f}")
                    else:
                        self.lg.warning(f"Invalid sweep config: {(smu_nplc/plf)+step_delay:.4f} > {1/steps:.4f}")
                else:
                    good = True
            else:
                good = True
        else:
            good = True
        return good

    def on_run_button(self, button):
        """Send run info to experiment orchestrator via MQTT."""
        assert self.conf_a, "Config not properly loaded."
        assert self.conf_a_id is not None, "Config ID not properly registered."
        if self.enable_zee:
            also_note = "ALSO BE PREPARED FOR POTENTIAL TABLE Z-DIRECTION MOTION!"
        else:
            also_note = ""

        if self.is_usb_smu_config_good(final_check=True) is False:
            return

        if self.move_warning(also_note=also_note) == Gtk.ResponseType.OK:
            self.b.get_object("run_but").set_sensitive(False)  # prevent multipress
            self.b.get_object("progress").set_visible(False)  # progress unknown now
            run_name = self.b.get_object("run_name").get_text()
            run_name = pathlib.Path(run_name)  # run name can now be a path

            datapkg = self.make_data_package()

            rundata = datapkg["rundata"]  # has a and b types in it

            # make legacy run data message
            rundata_compat = {"args": rundata["conf_b"], "config": rundata["conf_a"]}
            jrundata_compatb = json.dumps(rundata_compat).encode()
            digest = hmac.digest(self.hk, jrundata_compatb, "sha1")
            rundata_compat["digest"] = f"0x{digest.hex()}"

            # TODO: mod the backend to use this
            # run_queue = get_things_to_measure(self.lg, rundata["conf_a"], rundata["conf_b"], datapkg["conf_c"])
            run_queue = get_things_to_measure(self.lg, rundata["conf_a"], rundata["conf_b"])

            msg = {}  # mqtt msg
            msg["cmd"] = "run"
            msg["rundata"] = rundata_compat
            msg["conf_a_id"] = self.conf_a_id
            msg["conf_b_id"] = None
            if self.mem_db_url:
                with redis.Redis.from_url(self.mem_db_url) as r:
                    conf_b_id = r.xadd("conf_bs", fields={"json": json.dumps(rundata["conf_b"])}, maxlen=100, approximate=True).decode()
                    msg["conf_b_id"] = conf_b_id

                    # mqtt publish the run message TODO: switch to memdb only
                    self.emit_message("measurement/run", json.dumps(msg), qos=2)

                    conf_c_id = r.xadd("conf_cs", fields={"json": json.dumps(datapkg["conf_c"])}, maxlen=100, approximate=True).decode()
                    rq_id = r.xadd("runqs", fields={"json": json.dumps(run_queue)}, maxlen=100, approximate=True).decode()
                    self.lg.log(29, f"Starting new run: {run_name}")

                    # new run event emission for memdb
                    new_run(r, self.conf_a_id, conf_b_id, conf_c_id, rq_id)

            try:
                autosave_config = self.conf_a["meta"]["autosave_enabled"] == True
            except:
                autosave_config = True

            if autosave_config == True:
                autosave_file_name = run_name.name + "_autosave.gsf.json"
                autosave_slots_file_name = run_name.name + "_autosave_slots.tsv"
                try:
                    user_autosave_path = self.conf_a["meta"]["autosave_path"]
                except:
                    user_autosave_path = "runconfigs"
                if pathlib.Path(user_autosave_path).is_absolute():
                    autosave_root = pathlib.Path(user_autosave_path)
                else:
                    autosave_root = pathlib.Path.home() / user_autosave_path
                autosave_pathname = autosave_root / run_name.parent

                autosave_pathname.mkdir(parents=True, exist_ok=True)
                autosave_destination = autosave_pathname / autosave_file_name
                tsv_autosave_destination = autosave_pathname / autosave_slots_file_name
                self.lg.log(29, f"Autosaving gui state to: {autosave_destination}")

                with open(tsv_autosave_destination, "w", newline="") as csvfile:
                    writer = csv.DictWriter(csvfile, fieldnames=datapkg["conf_c"][0].keys(), dialect="excel-tab")
                    writer.writeheader()
                    [writer.writerow(row) for row in datapkg["conf_c"]]

                with open(autosave_destination, "w") as f:
                    json.dump(datapkg, f)

    # makes the gui dict more consumable for a backend
    def mk_conf_b(self, gui_dict):
        args = {}

        # send almost every gui item up
        for key, val in gui_dict.items():
            # don't send up the stores, those are only for the gui
            # and their data is in the dataframes handled below
            if not (key.endswith("store") or (key == "digest")):
                args[key] = val["value"]

        # make current limit user set a bit easier for backend
        if gui_dict["i_lim_units"]["value"] == 0:  # user selected mA units
            args["imax"] = gui_dict["i_lim"]["value"] / 1000  # scale mA to A and send up
            args["jmax"] = None
        elif gui_dict["i_lim_units"]["value"] == 1:  # user selected mA/cm^2 units
            args["imax"] = None
            args["jmax"] = gui_dict["i_lim"]["value"]  # send directly up units of mA/cm^2
        else:
            raise (ValueError("Current limit units not understood"))

        # do some scaling and selection logic for the backend
        if args["ad_switch"] == True:
            args["source_delay"] = -1
        args["chan1"] = args["chan1_ma"] / 1000
        args["chan2"] = args["chan2_ma"] / 1000
        args["chan3"] = args["chan3_ma"] / 1000
        args["i_dwell_value"] = args["i_dwell_value_ma"] / 1000

        # so that the data consumer knows what to look out for
        args["pixel_data_object_names"] = []

        # whitelist some pixel data frame cols for the saver.
        # only these cols should get saved to file
        saver_whitelist_cols = []
        saver_whitelist_cols.append("slot")
        saver_whitelist_cols.append("pad")
        saver_whitelist_cols.append("user_label")
        saver_whitelist_cols.append("layout")
        saver_whitelist_cols.append("area")
        saver_whitelist_cols.append("dark_area")
        # saver_whitelist_cols += self.variables

        # include the enable/disables
        args["enable_stage"] = self.enable_stage
        # args["enable_eqe"] = self.enable_eqe
        args["enable_iv"] = self.enable_iv
        # args["enable_lia"] = self.enable_lia
        # args["enable_mono"] = self.enable_mono
        # args["enable_psu"] = self.enable_psu

        # the potential pixel data objects we'll send (empty ones are not sent)
        # package up the data for each pixel
        unvars = []
        for bd in [self.iv_store.bd]:
            name = list(bd)[0]
            if len(bd[name]["slot"]) > 0:  # only send ones with selected pixels
                key = f"{name}_stuff"

                # # reorder the dataframe so that it matches the user's custom measurement order request
                # if self.custom_run_order_enabled == True:
                #     sort_metric = lambda x: [self.custom_run_order.index(e) if e in self.custom_run_order else np.NaN for e in x]
                #     df_sorted = df.sort_values("sort_string", key=sort_metric)
                #     args[key] = df_sorted
                # else:

                # swap blank user labels for uuid4s
                for lkey, val in bd.items():
                    if "user_label" in val:
                        labels = val["user_label"]
                        blanks = sum([l == "" for l in labels])
                        if blanks:
                            self.lg.warning(f"Empty substrate labels will be replaced with version 4 UUIDs in this data set.")
                            for i, lbl in enumerate(labels):
                                if lbl == "":
                                    bd[lkey]["user_label"][i] = str(uuid.uuid4())

                args[key] = bd
                args["pixel_data_object_names"].append(key)
                # TODO: add the variable cols to whitelist here

                # for debugging
                # print(df.to_markdown())
            if bd[name]["user_vars"]:
                unvars += [key for key, val in bd[name]["user_vars"][0].items()]

        saver_whitelist_cols += list(set(unvars))
        args["pix_cols_to_save"] = saver_whitelist_cols
        args["version"] = importlib.metadata.version(msui.__name__)

        jargsb = json.dumps(args).encode()
        digest = hmac.digest(self.hk, jargsb, "sha1")
        args["digest"] = f"0x{digest.hex()}"

        return args

    # called on right click to log before the menu is drawn
    def on_log_pre_popup(self, text_view, menu):
        sep = Gtk.SeparatorMenuItem()
        sep.set_visible(True)
        menu.prepend(sep)
        clmi = Gtk.MenuItem()
        clmi.set_label("Clear Log")
        clmi.set_visible(True)
        clmi.connect("activate", self.clear_log)
        menu.prepend(clmi)

    def clear_log(self, widget):
        self.logTB.set_text("")

    def on_smart_mode_activate(self, button):
        self.update_gui()

    def on_restart_services_button(self, button):
        self.do_restart_services()

    def do_restart_services(self, *args, **kw_args):
        sdb = Sysdbus()
        sdb.connect()
        if sdb.ready == True:
            self.lg.log(29, "Restarting services.")
            for service in self.services_to_launch:
                sdb.restart_service(service)
        else:
            self.lg.log(29, "Not restarting services.")

    def on_stop_services_button(self, button):
        sdb = Sysdbus()
        sdb.connect()
        if sdb.ready == True:
            self.lg.log(29, "Stopping services.")
            for service in self.services_to_launch:
                sdb.stop_service(service)
        else:
            self.lg.log(29, "Not stopping services.")

    def on_start_services_button(self, button):
        sdb = Sysdbus()
        sdb.connect()
        if sdb.ready == True:
            self.lg.log(29, "Starting services.")
            for service in self.services_to_launch:
                sdb.start_service(service)
        else:
            self.lg.log(29, "Not starting services.")

    # handle changes in stack
    def on_stack_change(self, stack, child):
        active_tab = stack.child_get_property(stack.get_visible_child(), "name")
        GLib.idle_add(self.max_pane)

        if active_tab == "live_plots":
            GLib.idle_add(self.load_live_data_webviews, True)
            self.live_data_webviews_loaded_once = True
        else:
            if self.live_data_webviews_loaded_once == True:
                GLib.idle_add(self.load_live_data_webviews, False)

        if active_tab == "custom":
            GLib.idle_add(self.load_custom_webview, True)
            self.custom_webview_loaded_once = True
        else:
            if self.custom_webview_loaded_once == True:
                GLib.idle_add(self.load_custom_webview, False)

        if active_tab == "overview":
            GLib.idle_add(self.draw_array)

    def handle_window_configure(self, widget, event):
        """fired whenever the window is moved or resized"""
        height_now = event.height
        if self.known_window_height != height_now:
            self.known_window_height = height_now
            GLib.idle_add(self.max_pane)

    def max_pane(self, *arg):
        """maximize the pane"""
        pane = self.b.get_object("pane")
        pane.set_position(pane.get_property("min-position"))  # move the pane handle to the top

    # pause/unpause plots
    def on_plotter_switch(self, switch, state):
        j_msg = json.dumps(not state)
        self.emit_message("plotter/pause", j_msg, qos=2)

    # invert voltage plots switch
    def on_voltage_switch(self, switch, state):
        j_msg = json.dumps(state)
        self.emit_message("plotter/invert_voltage", j_msg, qos=2)

    # invert current plots switch
    def on_current_switch(self, switch, state):
        j_msg = json.dumps(state)
        self.emit_message("plotter/invert_current", j_msg, qos=2)

    # reads various gui item states and sets others accordingly
    # def needs to be called after loading a gui state file
    def update_gui(self, *args):
        # self.lg.debug("Updating gui...")
        if self.b.get_object("ad_switch").get_active():
            self.b.get_object("sd_lab").set_sensitive(False)
            self.b.get_object("source_delay").set_sensitive(False)
            self.b.get_object("source_delay").set_visibility(False)
            self.b.get_object("sd_dt").set_sensitive(False)
        else:
            self.b.get_object("sd_lab").set_sensitive(True)
            self.b.get_object("source_delay").set_sensitive(True)
            self.b.get_object("source_delay").set_visibility(True)
            self.b.get_object("sd_dt").set_sensitive(True)

        if self.b.get_object("return_switch").get_active():
            self.b.get_object("sweep_check").set_label("Step 2: I-V Sweeps")
        else:
            self.b.get_object("sweep_check").set_label("Step 2: I-V Sweep")

        me = self.b.get_object("i_dwell_check")
        parent = me.get_parent()
        if me.get_active():
            for sib in parent.get_children():
                sib.set_sensitive(True)
        else:
            for sib in parent.get_children():
                sib.set_sensitive(False)
            me.set_sensitive(True)

        me = self.b.get_object("sweep_check")
        parent = me.get_parent()
        if me.get_active():
            for sib in parent.get_children():
                sib.set_sensitive(True)
        else:
            for sib in parent.get_children():
                sib.set_sensitive(False)
            me.set_sensitive(True)

        me = self.b.get_object("v_dwell_check")
        parent = me.get_parent()
        if me.get_active():
            for sib in parent.get_children():
                sib.set_sensitive(True)
        else:
            for sib in parent.get_children():
                sib.set_sensitive(False)
            me.set_sensitive(True)

        me = self.b.get_object("mppt_check")
        parent = me.get_parent()
        if me.get_active():
            for sib in parent.get_children():
                sib.set_sensitive(True)
        else:
            for sib in parent.get_children():
                sib.set_sensitive(False)
            me.set_sensitive(True)


def main():
    app = App()
    app.run(sys.argv)  # TODO: switch this to argparse


if __name__ == "__main__":
    main()
