import logging
import traceback
import msui
import redis
import json


def new_run(r: redis.Redis, conf_a_id: str, conf_b_id: str, conf_c_id: str, rq_id: str) -> str:
    """register a new measurement run"""
    run_mapping = {}
    run_mapping["conf_a_id"] = conf_a_id
    run_mapping["conf_b_id"] = conf_b_id
    run_mapping["conf_c_id"] = conf_c_id
    run_mapping["rq_id"] = rq_id
    run_mapping["frontend_ver"] = msui.__version__
    # run_mapping["complete"] = 0
    # run_mapping["started"] = 0

    return r.xadd("runs", {"json": json.dumps(run_mapping)}, maxlen=100, approximate=True).decode()


def get_things_to_measure(lg: logging.Logger, conf_a: dict, conf_b: dict, conf_c: dict | None = None) -> list[list[dict]]:
    """tabulate a list of items to loop through during the measurement"""
    # int("checkerberrycheddarchew")  # force crash for testing

    center = conf_a["motion"]["centers"]["solarsim"]
    run_q = []

    if conf_c:  # WIP, not yet active
        # int("checkerberrycheddarchew")  # force crash for testing
        required_cols = ["slot", "user_label", "layout", "bitmask"]
        validated = []  # holds validated slot data
        # the client sent unvalidated slots data
        # validate it and overwrite the origional slots data
        try:
            listlist: list[list[str]] = conf_c["slots"]
            # NOTE: this validation borrows from that done in runpanel
            col_names = listlist.pop(0)
            variables = col_names.copy()
            for rcol in required_cols:
                assert rcol in col_names
                variables.remove(rcol)
            # checkmarks = []  # list of lists of bools that tells us which pixels are selected
            # user_labels = []  # list of user lables for going into the device picker store
            # layouts = []  # list of layouts for going into the device picker store
            # areas = []  # list of layouts for going into the device picker store
            # pads = []  # list of layouts for going into the device picker store
            for i, data in enumerate(listlist):
                assert len(data) == len(col_names)  # check for missing cells
                slot = data[col_names.index("slot")]
                assert slot == list(conf_a["slots"].keys())[i]  # check system label validity
                layout = data[col_names.index("layout")]
                assert layout in conf_a["substrates"]["layouts"]  # check layout name validity
                bm_val = int(data[col_names.index("bitmask")].removeprefix("0x"), 16)
                # li = self.layouts.index(layout)  # layout index
                # subs_areas = self.areas[li]  # list of pixel areas for this layout
                # assert 2 ** len(subs_areas) >= bm_val  # make sure the bitmask hex isn't too big for this layout
                if bm_val == 0:
                    continue  # nothing's selected so we'll skip this row
                # subs_pads = self.pads[li]  # list of pixel areas for this layout
                bitmask = bin(bm_val).removeprefix("0b")
                # subs_cms = [x == "1" for x in f"{bitmask:{'0'}{'>'}{len(subs_areas)}}"][::-1]  # checkmarks (enabled pixels) for this substrate
                user_label = data[col_names.index("user_label")]
                # checkmarks.append(subs_cms)
                datalist = []
                datalist.append(slot)
                datalist.append(user_label)
                # user_labels.append(user_label)
                datalist.append(layout)
                datalist.append(bitmask)
                # layouts.append(layout)
                # areas.append(subs_areas)
                # pads.append(subs_pads)

                for variable in variables:
                    datalist.append(data[col_names.index(variable)])
                validated.append([str(i), datalist])

        except Exception as e:
            # raise ValueError(f"Failed processing user-crafted slot table data: {repr(e)}")
            lg.error(f"Failed processing user-crafted slot table data: {repr(e)}")
            # log the exception's whole call stack for debugging
            tb = traceback.TracebackException.from_exception(e)
            lg.debug("".join(tb.format()))
        else:
            # TODO: use validated slot data to update stuff
            pass

    if "IV_stuff" in conf_b:
        stuff = conf_b["IV_stuff"]  # dict from dataframe
        stuff_name = next(iter(stuff.keys()))
        bd = stuff[stuff_name]

        # build pixel/group queue for the run
        if len(conf_a["smus"]) > 0:  # multismu case
            grouping = conf_a["slots"]["group_order"]
            for group in grouping:
                group_list = []
                for smu_index, device in enumerate(group):
                    sort_slot, sort_pad = device  # the slot, pad to sort on

                    for i, slot in enumerate(bd["slot"]):
                        if (slot, bd["pad"][i]) == (sort_slot, sort_pad):  # we have a match
                            pixel_dict = {}
                            pixel_dict["smui"] = smu_index
                            pixel_dict["layout"] = bd["layout"][i]
                            pixel_dict["slot"] = bd["slot"][i]
                            pixel_dict["device_label"] = bd["device_label"][i]
                            pixel_dict["user_label"] = bd["user_label"][i]
                            pixel_dict["pad"] = bd["pad"][i]
                            por = [float("nan") if x is None else x for x in bd["pixel_offset_raw"][i]]  # convert Nones to NaNs
                            sor = [float("nan") if x is None else x for x in bd["substrate_offset_raw"][i]]  # convert Nones to NaNs
                            pixel_dict["pos"] = [c - s - p for c, s, p in zip(center, sor, por)]
                            pixel_dict["mux_sel"] = (pixel_dict["slot"], pixel_dict["pad"])

                            area = bd["area"][i]
                            # handle custom area/dark area
                            if area == -1:
                                headings = list(bd.keys())
                                headings.remove("area")
                                headings.remove("dark_area")
                                for heading in headings:
                                    if "area" in heading.lower():
                                        if "dark" not in heading.lower():
                                            try:
                                                area = float(bd[heading][i])
                                                lg.log(29, f'Using user supplied area = {area} [cm^2] for slot {pixel_dict["slot"]}, pad# {pixel_dict["pad"]}')
                                            except:
                                                pass

                            dark_area = bd["dark_area"][i]
                            if dark_area == -1:  # handle custom dark area
                                headings = list(bd.keys())
                                headings.remove("area")
                                headings.remove("dark_area")
                                for heading in headings:
                                    if "area" in heading.lower():
                                        if "dark" in heading.lower():
                                            try:
                                                dark_area = float(bd[heading][i])
                                                lg.log(29, f'Using user supplied dark area = {dark_area} [cm^2] for slot {pixel_dict["slot"]}, pad# {pixel_dict["pad"]}')
                                            except:
                                                pass

                            # handle the cases where the user didn't tell us an area
                            if (area == -1) and (dark_area == -1):
                                area = 1.0
                                dark_area = 1.0
                                lg.warning(f'Assuming area = {area} [cm^2] for slot {pixel_dict["slot"]}, pad# {pixel_dict["pad"]}')
                                lg.warning(f'Assuming dark area = {dark_area} [cm^2] for slot {pixel_dict["slot"]}, pad# {pixel_dict["pad"]}')
                            elif area == -1:
                                area = dark_area
                                lg.warning(f'Assuming area = {area} [cm^2] for slot {pixel_dict["slot"]}, pad# {pixel_dict["pad"]}')
                            elif dark_area == -1:
                                dark_area = area
                                lg.warning(f'Assuming dark area = {dark_area} [cm^2] for slot {pixel_dict["slot"]}, pad# {pixel_dict["pad"]}')

                            pixel_dict["area"] = area
                            pixel_dict["dark_area"] = dark_area

                            group_list.append(pixel_dict)
                if len(group_list) > 0:
                    run_q.append(group_list)

    # disable turbo (parallel, multi smu) mode by unwrapping the groups
    if ("turbo_mode" in conf_b) and (conf_b["turbo_mode"] == False):
        unwrapped_run_queue = []
        for group in run_q:
            for pixel_dict in group:
                unwrapped_run_queue.append([pixel_dict])
        run_q = unwrapped_run_queue  # overwrite the run_queue with its unwrapped version

    return run_q
